# Moon game #


## Setup whole project

1. Run `lando start` to build backend for the project. That will install mysql, php and other 
needed containers. See `.lando.yml` file. (If there is some issues with starting lando, need to
run `lando poweroff` and then start again).
2. Run `./build.sh new` to setup whole project (backend and frontend) - run composer, import db.
3. After building is ended host http://localhost/ should be available (also on http://planet.game - on production). To make sure that 
basic backend is running, open `http://localhost/hello/world`. You should see "Hello, world"
in response.
5. Go to `backend/vendor/andsel/moquette/bin/` and run `nohup ./moquette.sh  > /dev/null &`. [TODO: NEED TO RUN AUTOMATICALLY]
6. Go to `backend/src/scripts/` and run `nohup php kernel.php --root > /dev/null &`. [TODO: NEED TO RUN AUTOMATICALLY]
7. Go to `backend/src/scripts/` and run `php status.php --remove_process=eventRunner`. [This will be added to kernel db at some point]