// Reads terrain from a binary file, generates an SQL script with INSERTs to the `terrain` table
// Launch this script from the console with:
//   node terrain-sql-generator.js <arguments>

// Default command line arguments for 200x200 temp terrain (2019-02-23):
// --cols 200 --file temp_terrain.bin --max_height=128 --outfile "../terrain.sql" --clear_db true

// Read the command line arguments
const argv = require('minimist')(process.argv.slice(2));

const BYTES_PER_CELL = 2;
let MAX_HEIGHT = 100; // What output height will correspond to the max value in the file (65535)

// Offset for rows and columns. Used when translating terrain (row, column) to SQL (row, column)
let ROW_OFFSET = 0;
let COL_OFFSET = 0;

// When true, make the middle of the terrain correspond to cell (0, 0) in SQL.
// When false, the top-left corner of the binary terrain will become (0, 0) in SQL
let CENTER = true;

let CLEAR_DB = false; // When true, clear all previous entries in terrain table

if (!argv.cols) {
    console.log("Must specify the number of grid columns as command line parameter: --cols ...");
    return;
}

if (!argv.outfile) {
    console.log("Must specify the output file path as command line parameter: --outfile ...");
    return;
}

if (!argv.file) {
    console.log("Must specify the file path as command line parameter: --file ...");
    return;
}

if (argv.max_height) {
    MAX_HEIGHT = argv.max_height;
}
const SCALE = MAX_HEIGHT / 65536;
console.log("Using max_height " + MAX_HEIGHT + ", scaling coef: " + SCALE);

if (argv.center !== undefined) {
    CENTER = argv.center ? true : false;
    console.log("Using max_height " + MAX_HEIGHT);
}

if (argv.clear_db !== undefined) {
    CLEAR_DB = argv.clear_db ? true : false;
}

if (CLEAR_DB) {
    console.log("SQL script will clear DB before inserts");
}

const COLS = argv.cols;
const FILENAME = argv.file;
const fs = require("fs");
const OUTFILE = argv.outfile;

console.log("Reading file", FILENAME);
let bytes = null;
try {
    bytes = fs.readFileSync(FILENAME);
} catch (e) {
    console.log("File not found!");
    return;
}

const BYTE_COUNT = bytes.byteLength;
const CELL_COUNT = BYTE_COUNT / BYTES_PER_CELL;
const ROWS = CELL_COUNT / COLS;

console.log(BYTE_COUNT + " bytes loaded");
console.log("Terrain size: " + ROWS + "x" + COLS);

if (CENTER) {
    ROW_OFFSET = -Math.floor(ROWS / 2) + 1;
    COL_OFFSET = -Math.floor(COLS / 2) + 1;
}

console.log("First terrain cell will become (" + COL_OFFSET + ", " + ROW_OFFSET + ") in SQL");

console.log("Writing output to file " + OUTFILE);
var outfile = fs.createWriteStream(OUTFILE);
if (CLEAR_DB) {
    outfile.write("TRUNCATE `terrain`;\n");
}
outfile.write("INSERT INTO `terrain` (row, col, nw, ne, sw, se) VALUES\n");

// Read cell heights one by one, generate output file
// We will use two terrain values for the corners of cells, therefore -1 in iteration
// We use two bytes per value
for (let row = 0; row < ROWS - 1; ++row) {
    for (let col = 0; col < COLS - 1; ++col) {
        let i = (row * COLS + col) * 2;
        // Get height values for the four corners
        let topLeft = bytes[i] + bytes[i + 1] * 256;
        let topRight = bytes[i + 2] + bytes[i + 3] * 256;
        i += COLS * 2; // Move index to the next row
        let bottomLeft = bytes[i] + bytes[i + 1] * 256;
        let bottomRight = bytes[i + 2] + bytes[i + 3] * 256;

        // Scale the values to the allowed range
        topLeft = Math.floor(topLeft * SCALE);
        topRight = Math.floor(topRight * SCALE);
        bottomLeft = Math.floor(bottomLeft * SCALE);
        bottomRight = Math.floor(bottomRight * SCALE);

        const r = row + ROW_OFFSET;
        const c = col + COL_OFFSET;

        // Will need a ; instead of , on the last insert
        let terminator = ",";
        const isLastRow = row === ROWS - 2;
        const isLastColumn = col === COLS - 2;
        // To avoid huge query, split it into several smaller inserts - one for every row
        if (isLastColumn) {
            terminator = ";";
        }
        outfile.write(`(${r}, ${c}, ${topLeft}, ${topRight}, ${bottomLeft}, ${bottomRight})${terminator}\n`);
        if (isLastColumn && !isLastRow) {
            // Start a new insert for a new row
            outfile.write("INSERT INTO `terrain` (row, col, nw, ne, sw, se) VALUES\n");
        }
    }

}
outfile.end();

console.log("Wait until script finishes!");