<?php

// Platform-specific configuration: platform.config.php
// This file should NOT be in the GIT. Instead, an example platform-config-template.php should be in the git
// Devs should take the template, fill in the local parameters
// Whenever someone updates any config parameters, the template file must be updated as well!

define("DEBUG", false); // Set this to true to enable some more debug output in dev environment
define("DB_HOST", "database");
define("DB_NAME", "moon_game");
define("DB_USER", "moon");
define("DB_PORT", "3306");
define("DB_PWD", "moon");
define("BACKEND_PATH", "/api"); // Backend path on the web server (relative part of the URL)

//////////////////////////////////////////////
// Constants - keep this part AS IS
//////////////////////////////////////////////
// LOG LEVELS
define('LOG_SWITCHED_OFF', 0);
define('LOG_DIRECT_OUTPUT', 1);
define('LOG_WRITE_FILE', 2);
define('LOG_WRITE_DB', 3);

//////////////////////////////////////////////
// Platform-specific part - adapt this to your platform
//////////////////////////////////////////////
// Database config.
define('KERNEL_DB_USER', 'moon');
define('KERNEL_DB_PASSWORD', 'moon');
define('KERNEL_DB_DBNAME', 'moon_game');
define('KERNEL_DB_HOST', 'database');
define('KERNEL_DB_PORT', 3306);

// Kernel config.
define('PROC_ROOT', "/app/backend/src/kernel/");
define('PROC_ROOT_CALLABLE', "/app/backend/src/scripts/");
define('PROC_NAMESPACE', "Moon\\Process\\"); // Namespace for processes to locate them.

// Parallel kernel process config.
define('PROC_EXECUTABLE', "php");
define('PROC_MAX_PROCESSES', 100); // for every proxy url
define('PROC_MAX_EXECUTION_TIME', 40);
define('PROC_SCRIPT_NAME', 'kernel.php');
define('PROC_SCRIPT_TIME_MAX_COUNT', 10); // What amount of script times to store.
define('PROC_MAX_CPU_USAGE', -1); // If -1 no checking of cpu limit check.
define('PROC_MAX_MEM_USAGE', -1); // If -1 no checking of mem limit check.

// Logging config.
define('LOG_DIRECTORY', '/var/log/');
define('LOG_LEVEL', LOG_DIRECT_OUTPUT); // 1 - direct output(echo), 2 - write into file, 3 - write into DB.
define('LOG_FILENAME', 'system_notices');
define('ERROR_LOG_FILENAME', 'system_errors');

// MQTT protocol config.
define('MQTT_HOST', "0.0.0.0");
define('MQTT_PORT', "1883");
define('MQTT_KEEPALIVE', "60");
define('MQTT_RECONNECT_DELAY', "10");
define('MQTT_KERNEL_TOPIC', "/kernel/command/");

// Timezone config.
date_default_timezone_set('Europe/Riga');