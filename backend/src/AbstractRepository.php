<?php

namespace Moon;

// Base class for repositories, provides common query functions
class AbstractRepository {
    /**
     * Run a select query which is supposed to return a single row
     * @param string $query SQL query, ? are allowed for param values
     * @param array $params parameters to be used instead of ? in the query
     * @return array|null Array with associative names or null on error
     */
    protected function execSingleRowQuery($query, $params) {
        $db = DbConn::getInstance();
        if (!$db) return null;
        $results = $db->execSelectQuery($query, $params);
        if ($results) {
            return mysqli_fetch_assoc($results);
        } else {
            return null;
        }
    }
}