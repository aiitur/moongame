<?php

namespace Moon;

// Handles different events
class EventController {
    /** Update state for units where building is done */
    public function updateUnitState() {
        $repo = UnitRepository::getInstance();
        return $repo->finishBuilding();
    }
}