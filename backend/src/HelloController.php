<?php

namespace Moon;


class HelloController {
    public function hello($params) {
        $msg = "world";
        if (isset($params["msg"])) {
            $msg = $params["msg"];
        }
        return "Hello, $msg";
    }
}