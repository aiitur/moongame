<?php

namespace Moon;

/**
 * Represents HTTP response
 */
class Response {
    /** @var string */
    private $body;

    /**
     * Response constructor.
     * @param string $body
     */
    public function __construct($body) {
        $this->body = $body;
    }

    public function getBody() {
        return $this->body;
    }
}