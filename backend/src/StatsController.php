<?php

namespace Moon;

// Reports player stats: score, etc
class StatsController {
    /**
     * Get total score for the player
     * @param array $params URL-encoded GET parameters
     * @param string $post_data HTTP POST body
     * @return false|string
     */
    public function getScore($params, $post_data) {
        $repo = StatsRepository::getInstance();
        return json_encode($repo->getScore());
    }

    /**
     * Get total energy production and consumption for the player
     * @param array $params URL-encoded GET parameters
     * @param string $post_data HTTP POST body
     * @return false|string
     */
    public function getEnergy($params, $post_data) {
        $repo = StatsRepository::getInstance();
        return json_encode($repo->getEnergy());
    }
}