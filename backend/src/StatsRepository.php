<?php

namespace Moon;

// Reports player stats-related data (score, etc)
class StatsRepository extends AbstractRepository {
    // Constructor allowed only internally, to implement the singleton access
    private function __construct() {
    }

    /** @var StatsRepository */
    private static $instance;

    /**
     * Get singleton instance of this class
     * @return StatsRepository
     */
    public static function getInstance() {
        if (!self::$instance) {
            self::$instance = new StatsRepository();
        }
        return self::$instance;
    }

    /**
     * Get total score that player has earned
     * @return int|bool Score for the player or false on error
     */
    public function getScore() {
        $query = "SELECT SUM(s.score) AS score FROM `units` AS u LEFT JOIN `unit_score` AS s ON u.unit_type = s.unit_type "
            . "WHERE u.state = ".UnitRepository::STATE_READY;
        $row = $this->execSingleRowQuery($query, []);
        if ($row) {
            return (int)$row["score"];
        } else {
            return false;
        }
    }

    /**
     * Get current energy production and consumption for the user
     * @param bool $onlyOperational When true, count only operational units. When false, include units under construction.
     * @return array|bool Array with ["production", "consumption"] or false on error
     */
    public function getEnergy($onlyOperational = true) {
        $query = "SELECT SUM(ut.power_cons) AS cons, SUM(ut.power_prod) AS prod FROM unit_types ut "
            . "INNER JOIN units u ON u.unit_type = ut.id";
        $params = [];
        if ($onlyOperational) {
            $query .= " WHERE u.state = ?";
            $params = [UnitRepository::STATE_READY];
        }
        $row = $this->execSingleRowQuery($query, $params);
        if ($row) {
            return ["production" => (int)$row["prod"], "consumption" => (int)$row["cons"]];
        } else {
            return false;
        }
    }

    /**
     * @return int Amount of power currently available (production minus consumption)
     */
    public function getEnergyBudget() {
        $energy = $this->getEnergy(false); // Count also units currently under construction
        if ($energy) {
            return (int)($energy["production"] - $energy["consumption"]);
        } else {
            return 0;
        }
    }
}