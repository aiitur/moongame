<?php

namespace Moon;

class UnitController {
    public function place($params, $post_data) {
        $data = json_decode($post_data);
        $errMsg = null;

        if (isset($data->row) && isset($data->col) && isset($data->type_id)) {
            list($errMsg, $buildTime) = $this->checkIfCanBuildUnit($data->row, $data->col, $data->type_id);
            if (!$errMsg && $buildTime > 0) {
                $repo = UnitRepository::getInstance();
                $errMsg = $repo->placeUnit($data->row, $data->col, $data->type_id, $buildTime);
            }
        } else {
            $errMsg = "Wrong POST data format";
        }

        if ($errMsg) {
            $ok = false;
            $errMsg = "Can not build: $errMsg";
        } else {
            $ok = true;
            $errMsg = "";
        }
        return ["ok" => $ok, "err" => $errMsg];
    }

    /**
     * Url callback to change unit status by given row, col, type.
     *
     * @param $params
     * @param $post_data
     * @return array
     */
    public function changeStatus($params, $post_data) {
        $data = json_decode($post_data);
        $errMsg = null;

        if (isset($data->row) && isset($data->col) && isset($data->type_id) && isset($data->status)) {
            $repo = UnitRepository::getInstance();
            $errMsg = $repo->changeStatus($data->row, $data->col, $data->type_id, $data->status);
        } else {
            $errMsg = "Wrong POST data format";
        }

        if ($errMsg) {
            $ok = false;
            $errMsg = "Can not change status: $errMsg";
        } else {
            $ok = true;
            $errMsg = "";
        }
        return ["ok" => $ok, "err" => $errMsg];
    }

    /**
     * Get all units
     * @return false|string JSON array, false on error
     */
    public function getAll() {
        $repo = UnitRepository::getInstance();
        return json_encode($repo->getAll());
    }

    /**
     * Get unit types
     * @return false|string JSON array, false on error
     */
    public function getTypes() {
        $repo = UnitRepository::getInstance();
        return json_encode($repo->getTypes());
    }

    /**
     * Delete a unit
     * @param $params
     * @return int number of units deleted, 0 on error
     */
    public function delete($params) {
        if (!isset($params["row"]) || !isset($params["col"])) {
            return 0;
        }

        $repo = UnitRepository::getInstance();
        return $repo->delete($params["row"], $params["col"]);
    }

    /**
     * Get build time for a unit of specific type
     * @return int Build time in seconds, -1 if unit can't be built
     */
    public function getBuildTime($params) {
        if (!isset($params["type"])) {
            return -1;
        }
        $unitTypeId = $params["type"];
        $repo = UnitRepository::getInstance();
        $typeInfo = $repo->getTypeInfo($unitTypeId);
        if (!$typeInfo) return -1;

        $statsrepo = StatsRepository::getInstance();
        $power = $statsrepo->getEnergyBudget();
        if ($power > 0 && $power >= $typeInfo["power_cons"] && $repo->isBuildingRobotAvailable()) {
            return UnitService::getBuildTime($typeInfo["build_time"], $typeInfo["build_energy"], $power);
        } else {
            return -1;
        }
    }

    /**
     * Check if a specific type of unit can be built at specific location
     * @param $row
     * @param $col
     * @param $unit_type_id
     * @return array [$errMsg, $buildTime] $errMsg = Error message if building is not possible, null if all is ok.
     *  $buildTime is buildTime required. Set only if $errMsg is null
     */
    private function checkIfCanBuildUnit($row, $col, $unit_type_id) {
        $errMsg = null;
        $buildTime = null;

        // Find out if unit type is correct
        $repo = UnitRepository::getInstance();
        $typeInfo = $repo->getTypeInfo($unit_type_id);
        if ($typeInfo) {
            // Find out if cell is explored
            $existing_unit = $repo->getUnitAt($row, $col);
            if ($existing_unit) {
                // Find out if the cell is free
                if ($existing_unit["unit_type"] == UnitRepository::TYPE_NONE) {
                    $statsrepo = StatsRepository::getInstance();
                    // Find out if there is enough energy
                    $power = $statsrepo->getEnergyBudget();
                    if ($power > 0) {
                        if ($power >= $typeInfo["power_cons"]) {
                            // Find out if no other building is in progress
                            if ($repo->isBuildingRobotAvailable()) {
                                $buildTime = UnitService::getBuildTime($typeInfo["build_time"], $typeInfo["build_energy"], $power);
                            } else {
                                $errMsg = "All building robots currently busy";
                            }
                        } else {
                            $errMsg = "Not enough energy to turn on the unit after it is built (need {$typeInfo['power_cons']}, have $power)";
                        }
                    } else {
                        $errMsg = "No energy available";
                    }
                } else {
                    $errMsg = "Cell is not empty";
                }
            } else {
                $errMsg = "Cell is not explored";
            }
        } else {
            $errMsg = "Wrong unit type";
        }

        return [$errMsg, $buildTime];
    }
}