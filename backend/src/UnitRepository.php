<?php

namespace Moon;

use \Datetime;

// Reports unit-related data
class UnitRepository extends AbstractRepository {
    // Constants for unit state IDs.
    const STATE_BUILDING = 1;
    const STATE_READY = 2;

    const TYPE_NONE = 0; // unit_type of an empty cell
    const TYPE_RADAR = 2; // @TODO - this need to be moved away to radar class?
    const RADAR_DEFAULT_RANGE = 3; // @TODO - this need to be moved away to radar class?

    // Constructor allowed only internally, to implement the singleton access
    private function __construct() {
    }

    /** @var UnitRepository */
    private static $instance;

    /**
     * Get singleton instance of this class
     * @return UnitRepository
     */
    public static function getInstance() {
        if (!self::$instance) {
            self::$instance = new UnitRepository();
        }
        return self::$instance;
    }

    /**
     * Try to place a unit at given row and column in the DB.
     * @param int $row Row on the grid
     * @param int $col Column on the grid
     * @param int $type Unit type ID
     * @param int $build_time How many seconds the build should take
     * @return string|null Error message, null if all ok
     */
    public function placeUnit($row, $col, $type, $build_time) {
        $db = DbConn::getInstance();
        if (!$db) return "No connection to DB";

        if ($build_time < 0) return "Invalid build time";

        // Try to insert the value - if it fails, then a unit is already placed there (unique constraint raises exception)
        $query = "UPDATE units SET unit_type = ?, state = ?, placed=NOW(), next_change = NOW() + INTERVAL ? SECOND WHERE row = ? AND col = ?";
        $state = $build_time > 0 ? self::STATE_BUILDING : self::STATE_READY;
        $num_rows = $db->execUpdateQuery($query, [$type, $state, $build_time, $row, $col]);
        if ($num_rows == 1) {
            $errmsg = null;
        } else {
            if (DEBUG) {
                $errmsg = DbConn::getLastError();
            } else {
                $errmsg = "Database constraints failed";
            }
        }
        return $errmsg;
    }

    /**
     * Change unit status.
     *
     * @param int $row Row on the grid
     * @param int $col Column on the grid
     * @param int $type Unit type ID
     * @param boolean $status Enabled or Disabled unit.
     * @return bool|string|null
     */
    public function changeStatus($row, $col, $type, $status) {
        $db = DbConn::getInstance();
        if (!$db) return "No connection to DB";

        $status = $status ? 1 : 0;

        // Try to insert the value - if it fails, then a unit is already placed there (unique constraint raises exception)
        $query = "UPDATE units SET status = ? WHERE row = ? AND col = ? AND unit_type = ?";

        $num_rows = $db->execUpdateQuery($query, [$status, $row, $col, $type]);
        if ($num_rows == 1) {
            $errmsg = null;
        } else {
            if (DEBUG) {
                $errmsg = DbConn::getLastError();
            } else {
                $errmsg = "Database constraints failed";
            }
        }
        return $errmsg;
    }

    /**
     * Insert empty unit into specific row, col with unit type.
     *
     * @param int $row Row on the grid
     * @param int $col Column on the grid
     * @return bool|string|null Error message, null if all ok
     */
    public function placeEmptyUnit($row, $col) {
        $db = DbConn::getInstance();
        if (!$db) return "No connection to DB";

        // Try to insert the value - if it fails, then a unit is already placed there (unique constraint raises exception)
        $query = "INSERT into units (row, col, unit_type) VALUES (?, ?, ?)";
        $num_rows = $db->execUpdateQuery($query, [$row, $col, self::TYPE_NONE]);
        if ($num_rows == 1) {
            $errmsg = null;
        } else {
            if (DEBUG) {
                $errmsg = DbConn::getLastError();
            } else {
                $errmsg = "Database constraints failed";
            }
        }
        return $errmsg;
    }

    /**
     * Function to enable/disable unit.
     *
     * @param int $row Row on the grid
     * @param int $col Column on the grid
     * @param int $status - 0 or 1.
     * @return bool|string|null
     */
    public function setUnitStatus($row, $col, $status) {
        $db = DbConn::getInstance();
        if (!$db) return "No connection to DB";

        $query = "UPDATE units SET status = ? WHERE row = ? AND col = ?";
        $num_rows = $db->execUpdateQuery($query, [$status, $row, $col]);
        if ($num_rows == 1) {
            $errmsg = null;
        } else {
            if (DEBUG) {
                $errmsg = DbConn::getLastError();
            } else {
                $errmsg = "Database constraints failed";
            }
        }
        return $errmsg;
    }

    /**
     * @return array|bool List of all units or false on error
     */
    public function getAll() {
        $db = DbConn::getInstance();
        if (!$db) return false;

        $units = [];
        $query = "SELECT u.row, u.col, u.status as unit_status, t.nw, t.ne, t.se, t.sw, u.unit_type, s.name AS state, u.next_change FROM units u "
            . "LEFT JOIN unit_states s ON s.id = u.state LEFT JOIN terrain t ON u.row = t.row AND u.col = t.col "
            . "ORDER BY row ASC, col ASC";
        $results = $db->execSelectQuery($query, []);
        if ($results) {
            foreach ($results as $item) {
                if (!$item["next_change"]) {
                    unset($item["next_change"]); // When item has no next state change scheduled, do not report it.
                } else {
                    if (!empty($item["next_change"])) {
                        $item["next_change"] = $this->calculateRemainingTime($item["next_change"]);
                    }
                }

                $units[] = $item;
            }
        }
        $server_time = date("Y-m-d H:i:s");
        return ["units" => $units, "server_time" => $server_time];
    }

    private function calculateRemainingTime($fullDate) {
        $now = new DateTime();
        $future_date = new DateTime($fullDate);

        $interval = $future_date->diff($now);

        return $interval->format("%a days, %h hours, %i minutes, %s seconds");
    }

    /**
     * Select only units with specific type and state.
     *
     * @param $type
     * @param int $state
     * @return array - list of unit entries.
     */
    public function getUnits($type, $state=self::STATE_BUILDING) {
        $db = DbConn::getInstance();
        if (!$db) return false;

        $query = "SELECT row, col FROM `units` WHERE state = ? AND unit_type = ? AND next_change <= NOW()";
        $results = $db->execSelectQuery($query, [$state, $type]);
        $allResults = [];
        foreach ($results as $item) {
            $allResults[] = $item;
        }

        return $allResults;
    }

    /**
     * Process array with radar positions and generate empty cells around them.
     *
     * @param array $finishedBuildings - array of ["row", "col"] with radar coordinates that just finished building.
     */
    public function exceedRadarRange($finishedBuildings) {
        if (empty($finishedBuildings)) {
            return;
        }

        foreach ($finishedBuildings as $unit) {
            if (isset($unit['row']) && isset($unit['col'])) {
                $this->writeRangeEmptyUnits($unit['row'], $unit['col']);
            }
        }
    }

    /**
     * Generate empty unit entries around newly built radar.
     *
     * @param $startingRow int - radar position x.
     * @param $startingCol int - radar positoon y.
     */
    private function writeRangeEmptyUnits($startingRow, $startingCol) {
        $rangeLeftCorner = $startingRow - self::RADAR_DEFAULT_RANGE;
        $rangeRightCorner = $startingRow + self::RADAR_DEFAULT_RANGE;

        $rangeTopCorner = $startingCol - self::RADAR_DEFAULT_RANGE;
        $rangeBottomCorner = $startingCol + self::RADAR_DEFAULT_RANGE;

        for($x = $rangeLeftCorner; $x < $rangeRightCorner + 1; $x++) {
            for ($y = $rangeTopCorner; $y < $rangeBottomCorner + 1; $y++) {
                // Skipping corners, for more realistic range look.
                if (($x == $rangeLeftCorner || $x == $rangeLeftCorner + 1) && $y == $rangeTopCorner ||
                    ($x == $rangeLeftCorner && $y == $rangeTopCorner + 1)) {
                    continue;
                }

                if (($x == $rangeRightCorner || $x == $rangeRightCorner - 1) && $y == $rangeTopCorner ||
                    ($x == $rangeRightCorner && $y == $rangeTopCorner + 1)) {
                    continue;
                }

                if (($x == $rangeRightCorner || $x == $rangeRightCorner - 1) && $y == $rangeBottomCorner ||
                    ($x == $rangeRightCorner && $y == $rangeBottomCorner - 1)) {
                    continue;
                }

                if (($x == $rangeLeftCorner || $x == $rangeLeftCorner + 1) && $y == $rangeBottomCorner ||
                    ($x == $rangeLeftCorner && $y == $rangeBottomCorner - 1)) {
                    continue;
                }

                $cellExists = $this->getUnitAt($x, $y);
                if (!$cellExists) {
                    $this->placeEmptyUnit($x, $y);
                }
            }
        }
    }

    /**
     * @return array|bool List of all unit types or false on error
     */
    public function getTypes() {
        $db = DbConn::getInstance();
        if (!$db) return false;

        $types = [];
        // ID=0 for empty cell
        $query = "SELECT * FROM unit_types WHERE id > 0";
        $results = $db->execSelectQuery($query, []);
        if ($results) {
            foreach ($results as $item) {
                if ($item["custom_props"]) {
                    // Parse custom props
                    $props = json_decode($item["custom_props"], true);
                    $item["custom_props"] = $props;
                } else {
                    unset($item["custom_props"]);
                }
                $types[] = $item;
            }
        }
        return $types;
    }

    /**
     * Delete a unit placed in specific row and column
     * @param $row
     * @param $col
     * @return int number of units deleted. 0 on error
     */
    public function delete($row, $col) {
        $db = DbConn::getInstance();
        if (!$db) return 0;

        return $db->execUpdateQuery("DELETE FROM units WHERE row = ? AND col = ?", [$row, $col]);
    }

    /**
     * Update state for units where building is done.
     * @return int Number of units updated.
     */
    public function finishBuilding() {
        $db = DbConn::getInstance();
        if (!$db) return 0;

        // Units that are about to be finished.
        $allRadarUnitsBefore = $this->getUnits(self::TYPE_RADAR);

        // Attempt to finish buildings.
        $query = "UPDATE units SET state = ?, next_change = NULL WHERE state = ? AND next_change <= NOW()";
        $result = $db->execUpdateQuery($query, [self::STATE_READY, self::STATE_BUILDING]);

        if (!empty($allRadarUnitsBefore)) {
            $this->exceedRadarRange($allRadarUnitsBefore);
        }

        return $result;
    }

    /**
     * Get info about one particular unit type
     * @param $unit_type_id
     * @return array|null
     */
    public function getTypeInfo($unit_type_id) {
        $query = "SELECT * FROM `unit_types` WHERE id = ?";
        return $this->execSingleRowQuery($query, [$unit_type_id]);
    }

    /**
     * Select units by row and col.
     *
     * @param int $row Row on the grid
     * @param int $col Column on the grid
     * @return array|null list with units.
     */
    public function getUnitAt($row, $col) {
        $query = "SELECT * FROM `units` WHERE row = ? AND col = ?";
        return $this->execSingleRowQuery($query, [$row, $col]);
    }


    /**
     * @return bool True if at least one building robot is currently available
     */
    public function isBuildingRobotAvailable() {
        $numRobots = $this->getBuildingRobotCount();
        $numBusy = $this->getCurrentBuildCount();
        $numAvailable = $numRobots - $numBusy;
        return $numAvailable > 0;
    }

    /**
     * @return int Total number of building robots for the user
     */
    public function getBuildingRobotCount() {
        $query = "SELECT `count` AS c FROM `robot_count`";
        $row = $this->execSingleRowQuery($query, []);
        return $row ? $row["c"] : 0;
    }

    /**
     * @return int Number of units currently under construction
     */
    public function getCurrentBuildCount() {
        $query = "SELECT COUNT(*) AS c FROM `units` WHERE `state` = ?";
        $row = $this->execSingleRowQuery($query, [self::STATE_BUILDING]);
        return $row ? $row["c"] : 0;
    }
}