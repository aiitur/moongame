<?php

namespace Moon;

// General Unit-related things that are not related to DB or HTTP requests
class UnitService {
    /**
     * Get time it takes to build a unit, in seconds
     * @param $buildTime int Minimum build time that is required for this unit, independent of available power
     * @param $buildEnergy int Amount of energy required to build this unit
     * @param $power int Currently available power budget (produced energy minus consumed energy)
     * @return int Build time in seconds, -1 if unit type is unknown
     */
    public static function getBuildTime($buildTime, $buildEnergy, $power) {
        if ($power <= 0) {
            return -1;
        }
        // Time based on energy requirement. kWh / kW = h. Conver from hours to seconds with * 3600
        $bte = (int) ceil($buildEnergy / $power * 3600);
        return max($buildTime, $bte);
    }
}