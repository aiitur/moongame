<?php

namespace Moon\Process;

use ProcessKernel\ProcessMain;
use Moon\EventController;

class EventRunner extends ProcessMain {
  public $process_interval   = 10; // Interval between calls in seconds.
  public $checkByCron        = false; // Flag to start process by cron definition variable $callScheduler.
  public $only_once          = true;
  public $forever_running    = false;
  public $max_execution_time = 5;
          
  function process() {
    $controller = new EventController();
    $num_updates = $controller->updateUnitState();
  }
}
