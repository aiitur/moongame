<?php

// This is a temporary solution for "kernel". Place it in cron, run every minute


namespace Moon;

chdir(__DIR__); // Make sure we are in the script dir
require '../../vendor/autoload.php';
require_once("../../platform_config.php");

$controller = new EventController();
$num_updates = $controller->updateUnitState();
//echo "$num_updates units updated\n";