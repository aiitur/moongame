<?php

namespace Moon;

require '../../vendor/autoload.php';
require_once("../../platform_config.php");

$repo = UnitRepository::getInstance();
//$res = $repo->placeUnit(2, 3, "tank");
//$num_deleted = $repo->delete(1, 1);
//echo "$num_deleted units deleted\n";
//$typeInfo = $repo->getTypeInfo(1);
//var_dump($typeInfo);
// var_dump($repo->getUnitAt(0, 100));

$ctrl = new UnitController();
$time = $ctrl->getBuildTime(["type" => 1]);
echo "Build time = $time";
