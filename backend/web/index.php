<?php

require '../vendor/autoload.php';
require "../platform_config.php";

use Moon\Router;

header('Access-Control-Allow-Origin: *');

// Respond to CORS preflights
// Source from https://www.dinochiesa.net/?p=754
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    // return only the headers and not the content
    header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE');
    header('Access-Control-Allow-Headers: origin, content-type, accept');
    header('Access-Control-Max-Age: 3600');
    exit;
}
//////////////////////////////////////////////

$router = new Router();
$response = $router->process();
if ($response !== false) {
    echo $response->getBody();
} else {
    header("HTTP/1.1 404 Invalid route");
}

function debug_log($msg) {
    echo $msg . "\n";
}
