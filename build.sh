#!/usr/bin/env bash

pushd `dirname $0` > /dev/null
ROOT=`pwd -P`
BACKENDPATH='backend'
FRONTENDPATH='frontend'
APPDIR='/app'

# Composer runs every time.
echo "Backend setup STARTED\n"
echo "Install composer packages.\n\n"
cd $ROOT/$BACKENDPATH
lando composer install

# Runs only if building new environment.
if [[ $1 == "new" ]]; then
    echo "Copy default config file to actual.\n\n"
    cp platform_config_template.php platform_config.php

    echo "Import base db.\n\n"
    lando php db-sync.php update

    echo "Import terrain sql.\n\n"
    lando db-import database/terrain.sql --no-wipe;
elif [[ $1 == "update" ]]; then
    # Run everything needed to update
    echo "Updating environment only"
    lando php db-sync.php update
else
    echo "\nOnly supported params for this script is 'new' or 'update'. For example ./build.sh update."
fi

echo "Copy kernel run files."
cp vendor/marisabols/processkernel/status.php src/scripts/status.php
cp vendor/marisabols/processkernel/kernel.php src/scripts/kernel.php

#echo "Backend setup FINISHED\n"

echo "Frontend setup STARTED\n"

echo "Install node packages.\n\n"

cd $ROOT/$FRONTENDPATH
lando npm install

echo "Creating production build\n\n"
lando npm run build

echo "Creating API link"
lando ssh -c "ln -s /app/backend/web/ /app/frontend/build/api"

echo "Copy default config file to development config.\n\n"
cp .env .env.development

echo "Frontend setup FINISHED\n"