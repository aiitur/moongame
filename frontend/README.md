# Frontend for Moon game
Must be connected to the backend.

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Getting started
To set up a local development environment :
* Make a copy of `.env` file, save it as `.env.development` 
* Add the same line as below in that file, and specify the necessary URL. 
For example, of the backend is at `https://yourdomain.com`, then set
`REACT_APP_BACKEND_URL=https://yourdomain.com`

To set up a production build, do the same, just save the settings in a file called `.env.production`.
