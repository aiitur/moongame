import * as Redux from 'redux';

// Redux storage of actual date for the page. Can be used to share events between components

const ACTIONS = {
    UNIT_SELECTED: "UNIT_SELECTED",
    ERROR: "ERROR",
    UNIT_INFO: "UNIT_INFO"
};

export function unitSelected(unit, isSelected) {
    if (!isSelected) {
        unit = null;
    }
    return {
        type: ACTIONS.UNIT_SELECTED,
        unit: unit,
    }
}

export function unitShowInfo(unit, isSelected) {
    return {
        type: ACTIONS.UNIT_INFO,
        unit: isSelected ? unit : null,
        isSelected: isSelected
    }
}

export function errorHappened(errMsg) {
    return {
        type: ACTIONS.ERROR,
        error: errMsg,
    }
}

export function clearError() {
    return errorHappened(null);
}

const EMPTY_STATE = {
    selectedUnit: null,
    error: null,
    selectedInfoUnit: null,
    onlyInfo: false
};

// Shallow copy of the object
function shallowCopy(src) {
    let o = {};
    return Object.assign(o, src);
}

// Reducer takes the current state and one action, returns the store state after the action
function reducer(state = EMPTY_STATE, action) {
    let newState = shallowCopy(state);
    switch (action.type) {
        case ACTIONS.UNIT_SELECTED:
            newState.selectedUnit = action.unit;
            break;
        case ACTIONS.ERROR:
            newState.error = action.error;
            break;
        case ACTIONS.UNIT_INFO:
            newState.selectedUnit = action.unit;
            newState.onlyInfo = action.isSelected;
            break;
        default:
            break;
    }

    return newState;
}

let reduxStore = Redux.createStore(reducer);
console.log("Redux store created");

export function dispatch(action) {
    reduxStore.dispatch(action);
}

export function getStoreState() {
    return reduxStore.getState();
}

export function subscribeToUpdates(eventCallback) {
    reduxStore.subscribe(eventCallback);
}

/**
 *
 * @param boolean onlyInfo - if need to display info, not select for building.
 * @returns {*}
 */
export function getSelectedUnit(onlyInfo=false) {
    const s = reduxStore.getState();

    if (!s.onlyInfo || onlyInfo) {
        return s.selectedUnit;
    }

    return null;
}

export function getLastError() {
    const s = reduxStore.getState();
    return s.error;
}