import radar_icon from "./assets/icons/radar.png";
import solar_panel_icon from "./assets/icons/solar.png";

const UNIT_ICONS = [
    {id: 1, n: "solar_panel", url: solar_panel_icon},
    {id: 2, n: "radar", url: radar_icon},
];

const MODEL_FILES = [
    {id: 1, n: "solar_panel", url: "SolarPanel"},
    {id: 2, n: "radar", url: "Radar"},
];

// Locates resource files
export default class ResourceLocator {
    /**
     * Get URL to icon for a particular unit
     * @param unit_type_id {number}
     * @returns {*} Resource URL
     */
    static getUnitIcon(unit_type_id) {
        return ResourceLocator._locateResource(unit_type_id, UNIT_ICONS);
    }

    /**
     * Get name of the 3D model file for a particular unit type
     * @param unit_type_id {number}
     * @returns {*} Model name, without path or extension
     */
    static getModelName(unit_type_id) {
        return ResourceLocator._locateResource(unit_type_id, MODEL_FILES);
    }

    /**
     * Locate a resource for a specific unit in a dictionary
     * @param tid {number} Unit type ID
     * @param resource_dictionary
     * @private
     * @returns {*} Resource URL
     */
    static _locateResource(tid, resource_dictionary) {
        let url = null;
        if (tid) {
            let i = 0;
            while (i < resource_dictionary.length && !url) {
                const ico = resource_dictionary[i];
                if (ico.id === tid) {
                    url = ico.url;
                }
                ++i;
            }
        }
        return url;
    }
}