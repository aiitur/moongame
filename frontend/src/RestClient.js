// This file contains all the REST API calls

// Get the backend API URL from configuration
const API_URL_BASE = process.env.REACT_APP_BACKEND_URL;

export default class RestClient {
    // Get unit types from backend, pass parsed results to callback function
    static getUnitTypes(callback) {
        fetch(API_URL_BASE + "/units/get_types").then(response => {
            return response.json();
        }).then(callback);
    }

    /**
     * Send request to the backend, try to place unit in given cell
     * Call callback function when response from backend comes: the parameter will be the response from the server
     * Use response.ok to check whether the response was successful
     *
     * @param row Index of row where to place the unit
     * @param col Index of column where to place the unit
     * @param unitType {string} Short name of the unit type
     * @param callback Function that will be called when response is received
     */
    static placeUnit(row, col, unitType, callback) {
        const api_url = API_URL_BASE + "/units/place";
        const data = {row: row, col: col, type_id: unitType};
        fetch(api_url, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(data)
        }).then(function(response) {
            return response.json();
        }).then(callback);
    }

    /**
     * Send request to the backend to change unit status.
     *
     * @param row Index of row where to place the unit
     * @param col Index of column where to place the unit
     * @param unitType {string} Short name of the unit type
     * @param status Enabled or Disabled
     * @param callback Function that will be called when response is received
     */
    static setUnitStatus(row, col, unitType, status, callback) {
        const api_url = API_URL_BASE + "/units/change_status";
        const data = {row: row, col: col, type_id: unitType, status: status};
        fetch(api_url, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(data)
        }).then(function(response) {
            return response.json();
        }).then(callback);
    }

    // Get units and explored terrain from backend, pass parsed results to callback function
    static getUnitsAndTerrain(callback) {
        fetch(API_URL_BASE + "/units/get_all").then(response => {
            return response.json();
        }).then(callback);
    }

    // Get total player score from backend, pass parsed result to callback function
    static getScore(callback) {
        fetch(API_URL_BASE + "/stats/get_score").then(response => {
            return response.json();
        }).then(callback);
    }

    // Get total player energy production and consumption, pass parsed result to callback function
    static getEnergy(callback) {
        fetch(API_URL_BASE + "/stats/get_energy").then(response => {
            return response.json();
        }).then(callback);
    }
}