import * as React from "react";
import RestClient from "../RestClient";

// Displays player energy consumption and production
export default class EnergyInfo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {production: null, consumption: null};
        // Binding needed so that "this" is accessible withing callbacks
        this.onEnergyLoaded = this.onEnergyLoaded.bind(this);
    }

    render() {
        let info = "Loading..."; // Show this while info not loaded
        if (this.state.production !== null && this.state.consumption !== null) {
            info =
                <div>
                    <div>Energy production: {this.state.production}</div>
                    <div>Energy consumption: {this.state.consumption}</div>
                </div>
            ;
        }
        return (
            <div id="energy-info" className="info-section">{info}</div>
        );
    }

    componentDidMount() {
        // Send REST request to backend, load score
        RestClient.getEnergy(this.onEnergyLoaded);
    }

    // Called when score returned from backend
    onEnergyLoaded(energy) {
        console.log("Energy loaded:");
        console.log(energy);
        if (energy && energy.production !== undefined && energy.consumption !== undefined) {
            this.setState({production: energy.production, consumption: energy.consumption});
        }
    }
}