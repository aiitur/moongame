import * as React from 'react';
import {clearError, dispatch, getStoreState, subscribeToUpdates} from "../ReduxStore";

export default class ErrorMessage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {error: ""};
        this.timer = null;

        // Binding to get access to "this" in callbacks
        this.onStoreStateChanged = this.onStoreStateChanged.bind(this);
    }

    componentDidMount() {
        // Receive Redux store state updates
        subscribeToUpdates(this.onStoreStateChanged);
    }

    render() {
        const e = this.state.error ? this.state.error : "";
        return <div id="error-container" className="info-section">{e}</div>;
    }

    /**
     * This method is called when Redux store state changes
     */
    onStoreStateChanged() {
        const error = getStoreState().error;
        if (this.state.error !== error) {
            this.setState({error: error});

            // If a timer is running, cancel it
            if (this.timer) {
                clearTimeout(this.timer);
            }
            // Hide error after a while
            if (error != null) {
                this.timer = setTimeout(hideError, 5000);
            }
        }
    }
}

function hideError() {
    dispatch(clearError());
}
