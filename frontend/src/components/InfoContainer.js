import * as React from "react";
import UnitInfo from "./UnitInfo";
import ScoreInfo from "./ScoreInfo";
import EnergyInfo from "./EnergyInfo";
import ErrorMessage from "./ErrorMessage";

// Displays an info bar: unit information, etc
export default class InfoContainer extends React.Component {
    render() {
        return (
            <div id="info-container">
                <UnitInfo/>
                <ScoreInfo/>
                <EnergyInfo/>
                <ErrorMessage/>
            </div>
        );
    }
}