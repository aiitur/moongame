import React from 'react';
import RestClient from "../RestClient";
import UnitIcon from "./UnitIcon";
import InfoContainer from "./InfoContainer";

// React component for the whole menu
export default class MenuContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {units: []};
        // Binding needed so that "this" is accessible withing callbacks
        this.onUnitsLoaded = this.onUnitsLoaded.bind(this);
    }

    render() {
        // Show unit icons or simple text when data not loaded
        const s = {width: this.props.width};
        let unit_icons = "Loading...";
        if (this.state.units) {
            unit_icons = [];
            for (let i = 0; i < this.state.units.length; ++i) {
                const u = this.state.units[i];
                unit_icons.push(<UnitIcon unit={u} key={"icon_" + u.short_name}/>);
            }
        }
        return (
            <div id="menu-container" style={s}>
                <div id="unit-menu">
                    {unit_icons}
                </div>
                <InfoContainer/>
            </div>
        );
    }

    componentDidMount() {
        // Fetch data from backend when component is ready
        RestClient.getUnitTypes(this.onUnitsLoaded);
    }

    onUnitsLoaded(unitTypes) {
        //console.log("Unit types loaded:", unitTypes);
        this.setState({units: unitTypes}); // Save the unit_types in the state. Will force render()
    }
}