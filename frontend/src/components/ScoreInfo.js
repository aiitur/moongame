import * as React from "react";
import RestClient from "../RestClient";

// Displays player score information
export default class ScoreInfo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {score: null};
        // Binding needed so that "this" is accessible withing callbacks
        this.onScoreLoaded = this.onScoreLoaded.bind(this);
    }

    render() {
        let info = "Loading..."; // Show this while info not loaded
        if (this.state.score) {
            info = <div>Current player score: {this.state.score}</div>;
        }
        return (
            <div id="score-info" className="info-section">{info}</div>
        );
    }

    componentDidMount() {
        // Send REST request to backend, load score
        RestClient.getScore(this.onScoreLoaded);
    }

    // Called when score returned from backend
    onScoreLoaded(score) {
        if (score) {
            console.log("Score loaded: " + score);
            this.setState({score: score});
        }
    }
}