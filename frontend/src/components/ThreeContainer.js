import React from 'react';
import ThreeEntryPoint from '../includes/ThreeEntryPoint';
import "../index.css"

// Size of the canvas where the whole ThreeJS world will be drawn
const CANVAS_WIDTH = 1000;
const CANVAS_HEIGHT = 800;

// The main React wrapper-component for the Three-js world
export default class ThreeContainer extends React.Component {
    componentDidMount() {
        new ThreeEntryPoint(this.threeRootElement, CANVAS_WIDTH, CANVAS_HEIGHT,
            this.props.offsetLeft, this.props.offsetTop);
    }

    render() {
        return (
            <div ref={element => this.threeRootElement = element} id="threejs-container"/>
        );
    }
}
