import * as React from "react";
import ResourceLocator from "../ResourceLocator";
import {dispatch} from "../ReduxStore.js"
import {getSelectedUnit, subscribeToUpdates, unitSelected} from "../ReduxStore";

// Displays one unit icon in the menu
export default class UnitIcon extends React.Component {
    constructor(props) {
        super(props);
        this.state = {selected: false};

        // Binding needed so that "this" is accessible withing callbacks
        this.onClick = this.onClick.bind(this);
        this.onStoreStateUpdated = this.onStoreStateUpdated.bind(this);

        // Get notifications on Redux store state updates
        subscribeToUpdates(this.onStoreStateUpdated);
    }

    render() {
        const u = this.props.unit;
        if (!u) {
            return "";
        }
        const icon_url = ResourceLocator.getUnitIcon(u.id);
        const s = {};
        var cssClass = "unit-icon";
        if (this.state.selected) {
            cssClass += " selected";
        }
        if (icon_url) {
            return <img alt={u.name} title={u.name} src={icon_url} className={cssClass} key={"icon_" + u.short_name}
                        onClick={this.onClick} style={s}/>;
        } else {
            return "";
        }
    }

    onClick() {
        let isSelected = this.checkIfSelected();

        // Dispat ch an action to Redux store. It will notify all listeners
        dispatch(unitSelected(this.props.unit, isSelected ? false : true));
    }

    // This method is called whenever Redux store state updates
    onStoreStateUpdated() {
        let isSelected = this.checkIfSelected();

        this.setState({selected: isSelected});
    }

    checkIfSelected() {
        let isSelected = false;
        const selectedUnit = getSelectedUnit(true);

        if (selectedUnit) {
            isSelected = selectedUnit.id === this.props.unit.id;
        }

        return isSelected;
    }
}
