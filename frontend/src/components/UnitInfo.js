import * as React from "react";
import {getSelectedUnit, subscribeToUpdates} from "../ReduxStore";
import RestClient from "../RestClient";

// Displays unit information
export default class UnitInfo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {unit: null};
        // Binding needed so that "this" is accessible withing callbacks
        this.onStoreStateChanged = this.onStoreStateChanged.bind(this);
        this.handleStatusChange = this.handleStatusChange.bind(this);
    }

    render() {
        let info = "Select a unit to see it's details"; // When no unit selected, show this
        if (this.state.unit) {
            const u = this.state.unit;
            // Power consumption and production
            let consumption = "";
            if (u.power_cons) {
                consumption = <p>Energy consumption when active: {u.power_cons}kW </p>;
            }
            let production = "";
            if (u.power_prod) {
                production = <p>Power production: {u.power_prod}kW </p>;
            }
            let next_change = "";
            if (u.cell_info && u.cell_info.next_change) {
                next_change = <p>Remaining building time: <br/><br/>{u.cell_info.next_change}</p>;
            }
            let status_switcher = "";
            if (u.status_switcher) {
                status_switcher = <p>Status: <input
                    name="status"
                    type="checkbox"
                    checked={u.cell_info.unit_status}
                    onChange={this.handleStatusChange} /></p>;
            }
            info = <div>
                <h2>{u.name}</h2>
                <p>{u.description}.</p>
                {consumption}
                {production}
                <p>Requirements for building:</p>
                <ul>
                    <li>Time: {Math.ceil(u.build_time / 60)} min</li>
                    <li>Energy: {u.build_energy} kWh</li>
                </ul>
                {next_change}
                {status_switcher}
            </div>;
        }

        return (
            <div id="unit-info">{info}</div>
        );
    }

    handleStatusChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;

        let temp_unit = this.state.unit;
        temp_unit.cell_info.unit_status = value;
        this.setState({unit: temp_unit});

        const u = this.state.unit;
        RestClient.setUnitStatus(u.cell_info.row, u.cell_info.col, u.cell_info.unit_type, value, this.onUnitStatusChanged);
    }

    onUnitStatusChanged() {

    }

    componentDidMount() {
        // Receive events when Redux store state changes
        subscribeToUpdates(this.onStoreStateChanged);
    }

    // Called when Redux store state changes
    onStoreStateChanged() {
        const selectedUnit = getSelectedUnit(true);

        if (selectedUnit) {
            this.setState({unit: selectedUnit});
        } else {
            this.setState({unit: null});
        }
    }
}