import * as THREE from 'three'
import GameObject from "./GameObject";

// Global variable for scene object.
let sceneLoader = null;

class Cell extends GameObject {
    // Material used on the surface of all cells
    static material = new THREE.MeshPhongMaterial({
        color: 'black',
    });

    constructor(scene, rowNumber, colNumber) {
        super();
        sceneLoader = scene;
        this.uuid = null;
        this.coordArray = [];
        this.selectionMesh = null;
        this.rowNumber = rowNumber;
        this.colNumber = colNumber;
        this.unit = null; // Unit placed on this cell
    }

    draw(cellWidth, cellHeight, coordArray) {
        let tempGeometry = new THREE.PlaneBufferGeometry(cellWidth, cellHeight);
        tempGeometry.rotateX(-Math.PI / 2);

        let tempVertices = tempGeometry.attributes.position.array;

        for (let i = 0; i < coordArray.length; i++) {
            tempVertices[i] = coordArray[i];
        }

        let tempPlane = new THREE.Mesh(tempGeometry, Cell.material);

        // Setting cell class uuid.
        this.uuid = tempPlane.uuid;

        // Wireframe.
        const material = new THREE.LineBasicMaterial({
            color: 'aqua',
        });

        const xTopRight = coordArray[0]; // X axis coord
        const yTopRight = coordArray[1]; // Y axis coord
        const zTopRight = coordArray[2]; // Z axis coord

        const xBottomRight = coordArray[3]; // X axis coord
        const yBottomRight = coordArray[4]; // Y axis coord
        const zBottomRight = coordArray[5]; // Z axis coord

        const xTopLeft = coordArray[6]; // X axis coord
        const yTopLeft = coordArray[7]; // Y axis coord
        const zTopLeft = coordArray[8]; // Z axis coord

        const xBottomLeft = coordArray[9]; // X axis coord
        const yBottomLeft = coordArray[10]; // Y axis coord
        const zBottomLeft = coordArray[11]; // Z axis coord

        const geometry = new THREE.Geometry();
        geometry.vertices.push(
            new THREE.Vector3(xTopRight, yTopRight + 0.06, zTopRight),
            new THREE.Vector3(xBottomRight, yBottomRight + 0.06, zBottomRight),
            new THREE.Vector3(xBottomLeft, yBottomLeft + 0.06, zBottomLeft),
            new THREE.Vector3(xTopLeft, yTopLeft + 0.06, zTopLeft),
        );

        // this.geometry = geometry;
        this.coordArray = coordArray;

        const line = new THREE.Line(geometry, material);
        tempPlane.add(line);

        sceneLoader.scene.add(tempPlane);
    }

    /**
     * Register that a unit is on the cell
     * @param unit
     */
    setUnit(unit) {
        this.unit = unit;
    }

    /**
     * Function to get unit ubject.
     * @returns unit object of type GameObject.
     */
    getUnit() {
        return this.unit;
    }

    removeUnit() {
        this.unit = null;
    }

    isEmpty() {
        return this.unit == null;
    }

    /**
     * Function for internal use to draw multiple squares to represent selection.
     *
     * @param highlightOnly - when true, draw a highlight, not a "selected" frame
     * @param squareWidth How wide square to draw (line thickness)
     * @returns {Group}
     * @private
     */
    _drawSelectionCell(highlightOnly = false, squareWidth = 3) {
        const color = highlightOnly ? "orange" : "red";
        const material = new THREE.LineBasicMaterial({
            color: color,
        });

        const xTopRight = this.coordArray[0]; // X axis coord
        const yTopRight = this.coordArray[1]; // Y axis coord
        const zTopRight = this.coordArray[2]; // Z axis coord

        const xBottomRight = this.coordArray[3]; // X axis coord
        const yBottomRight = this.coordArray[4]; // Y axis coord
        const zBottomRight = this.coordArray[5]; // Z axis coord

        const xTopLeft = this.coordArray[6]; // X axis coord
        const yTopLeft = this.coordArray[7]; // Y axis coord
        const zTopLeft = this.coordArray[8]; // Z axis coord

        const xBottomLeft = this.coordArray[9]; // X axis coord
        const yBottomLeft = this.coordArray[10]; // Y axis coord
        const zBottomLeft = this.coordArray[11]; // Z axis coord

        let _innerStep = 0.02;
        let innerStep = 0;
        const group = new THREE.Group();

        for (let i = 0; i < squareWidth; i++) {
            innerStep = _innerStep * i;

            const geometry = new THREE.Geometry();
            geometry.vertices.push(
                new THREE.Vector3(xTopRight + innerStep, yTopRight + 0.06, zTopRight + innerStep),
                new THREE.Vector3(xBottomRight - innerStep, yBottomRight + 0.06, zBottomRight + innerStep),
                new THREE.Vector3(xBottomLeft - innerStep, yBottomLeft + 0.06, zBottomLeft - innerStep),
                new THREE.Vector3(xTopLeft + innerStep, yTopLeft + 0.06, zTopLeft - innerStep),
                new THREE.Vector3(xTopRight + innerStep, yTopRight + 0.06, zTopRight + innerStep),
            );

            const line = new THREE.Line(geometry, material);

            group.add(line);
        }

        return group;
    }

    /**
     * Function to draw cell selection square.
     * @param withoutSelection - if need just to set state, not draw.
     */
    drawSelection(withoutSelection=false) {
        // If highlight mesh exists, remove it. Selection mesh has higher priority
        if (this.selectionMesh) {
            this.removeHighlight();
        }
        this.selectionMesh = this._drawSelectionCell(false);
        sceneLoader.scene.add(this.selectionMesh);

        // Check if cell have unit on it.
        // No need to select again child object, if call came from that object already,
        if (!withoutSelection && !this.isEmpty()) {
            // Select also child element/unit.
            this.unit.select(true);
        }
    }

    /**
     * Function to draw cell highlight square.
     */
    drawHighlight() {
        // Don't create another highlight mesh if one exists already (perhaps the existing one is selection mesh)
        if (!this.selectionMesh) {
            this.selectionMesh = this._drawSelectionCell(true);
            sceneLoader.scene.add(this.selectionMesh);
        }
    }

    /**
     * Function to remove cell selection and free objects.
     */
    removeHighlight(eventName) {
        if (this.selectionMesh) {
            sceneLoader.scene.remove(this.selectionMesh);

            delete this.selectionMesh;
            this.selectionMesh = null;

            // Process deselection of unit only when click event is happening.
            if (eventName === 'click') {
                // Remove selection also from the child element/unit.
                if (!this.isEmpty() && this.unit.isSelected()) {
                    this.unit.deselect(eventName);
                }
            }
        }
    }

    /**
     * Return array with [x, y, z] coordinates of the center of this cell.
     * @returns {number[]}
     */
    getCenterLocation() {
        const avgX = (this.coordArray[0] + this.coordArray[3] + this.coordArray[6] + this.coordArray[9]) / 4;
        const avgY = (this.coordArray[1] + this.coordArray[4] + this.coordArray[7] + this.coordArray[10]) / 4;
        const avgZ = (this.coordArray[2] + this.coordArray[5] + this.coordArray[8] + this.coordArray[11]) / 4;
        return new THREE.Vector3(avgX, avgY, avgZ);
    }
}

export default Cell;
