// Different selection states that a game object can have
const SELECTION_TYPES = {
    NONE: 0, // Object is not selected, not highlighted
    SELECTED: 1, // Object is currently selected
    HIGHLIGHT: 2 // Object is highlighted but not selected (perhaps on mouse over)
};

export default class GameObject {
    constructor() {
        /** @private @type int */
        this.selectionState = SELECTION_TYPES.NONE;
    }

    // TODO - anotation for this function.
    setMaterialOpacity(unitMesh, opacity) {
        // We can't change opacity of the material directly. That would change transparency for all units of the
        // type who share the material. Therefore, we create a clone for the material and then update it.
        // We have to update material for all the mesh objects
        unitMesh.traverse(function (childMesh) {
            const transparentMaterial = childMesh.material.clone();
            transparentMaterial.opacity = opacity;
            transparentMaterial.transparent = true;
            childMesh.material = transparentMaterial;
        });
    }

    /**
     * Mark the object as selected.
     * @param withoutSelection - if need just to set state, not draw.
     */
    select(withoutSelection=false) {
        this.selectionState = SELECTION_TYPES.SELECTED;
        this.drawSelection(withoutSelection);
    }

    /**
     * Mark the object as highlighted (not selected)
     */
    highlight() {
        if (this.selectionState !== SELECTION_TYPES.SELECTED) {
            // Ignore highlight when there is a selection already (higher priority)
            this.selectionState = SELECTION_TYPES.HIGHLIGHT;
            this.drawHighlight();
        }
    }

    /**
     * Mark the object as neither selected, nor highlighted.
     * @param string eventName - name of what event callback this is called.
     */
    deselect(eventName='click') {
        this.selectionState = SELECTION_TYPES.NONE;
        this.removeHighlight(eventName);
    }


    /**
     * Return true when the object is selected
     * @returns {boolean}
     */
    isSelected() {
        return this.selectionState === SELECTION_TYPES.SELECTED;
    }

    /**
     * Return true when object's state is "highlighted but not selected"
     * @returns {boolean}
     */
    isHighlighted() {
        return this.selectionState === SELECTION_TYPES.HIGHLIGHT;
    }

    /**
     * Returns true if the object is not selected in any way (no highlight either)
     * @returns {boolean}
     */
    hasNoSelection() {
        return this.selectionState === SELECTION_TYPES.NONE;
    }

    /**
     * Draw selection highlight of the object. Must be implemented in child classes
     * @param withoutSelection - if need just to set state, not draw.
     * @abstract
     */
    drawSelection(withoutSelection=false) {
        throw new Error("drawSelection not implemented!");
    }

    /**
     * Draw simple highlight of the object. Must be implemented in child classes
     * @abstract
     */
    drawHighlight() {
        throw new Error("drawHighlight not implemented!");
    }

    /**
     * Clear highlight (or selection) of the object. Must be implemented in child classes
     * @abstract
     */
    removeHighlight() {
        throw new Error("removeHighlight not implemented!");
    }
}