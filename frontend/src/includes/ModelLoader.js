import ResourceLocator from "../ResourceLocator";
import GLTFLoader from 'three-gltf-loader';

// IDs of Unit types for which we want to load the models
const MODEL_TYPE_IDS = [1, 2];

/**
 * Loads the 3D models from .GLTF files
 */
export default class ModelLoader {
    static _instance = null; // Reference to singleton loader

    /**
     * Do NOT call this constructor directly! Instead, use ModelLoader.getInstance()!
     */
    constructor() {
        ModelLoader._instance = this;
        this.models = [];

        // Bind methods so that "this" is accessible in callbacks
        this._loadNextModel = this._loadNextModel.bind(this);
        this.onModelLoaded = this.onModelLoaded.bind(this);
    }

    /**
     * Get a singleton instance of this class
     * @returns {*}
     */
    static getInstance() {
        if (!ModelLoader._instance) {
            ModelLoader._instance = new ModelLoader();
        }
        return ModelLoader._instance;
    }

    /**
     * Start loading of the 3D models. Call the callback when done
     *
     * @param callback {function} Callback method to call when loading is done
     */
    load(callback) {
        this.callback = callback;
        if (MODEL_TYPE_IDS.length === 0) {
            console.log("No models to load");
        } else {
            this.nextModelIndex = 0;
            this._loadNextModel();
        }
    }

    /**
     * Load the next model (one model)
     * @private
     */
    _loadNextModel() {
        if (this.nextModelIndex < MODEL_TYPE_IDS.length) {
            const unitType = MODEL_TYPE_IDS[this.nextModelIndex];
            const modelName = ResourceLocator.getModelName(unitType);
            this.loadModelMesh(unitType, modelName, this.onModelLoaded);
        }
    }

    onModelLoaded(objMesh) {
        //objMesh.geometry.computeBoundingBox();
        this.nextModelIndex++;
        if (this.nextModelIndex < MODEL_TYPE_IDS.length) {
            this._loadNextModel();
        } else {
            console.log("All 3D models loaded");
            // Notify the callback that loading is done
            if (this.callback) {
                this.callback();
            }
        }
    }

    /**
     * Translate unit type to a key that can be used within this.models object.
     * @param unitType
     * @returns {string}
     * @private
     */
    static _getModelKey(unitType) {
        return "m" + unitType;
    }

    saveModel(unitType, model) {
        const key = ModelLoader._getModelKey(unitType);
        this.models[key] = model;
    }

    loadModelMesh(unitType, modelName, callback) {
        const loader = new GLTFLoader();
        const dir = "/models/" + modelName + "/";
        const filePath = dir + modelName + ".gltf";
        loader.load(filePath, function (gltf) {
            console.log("Model loaded:", filePath);
            let meshFound = false; // Load the first mesh only
            gltf.scene.traverse(function (child) {
                if (child.isMesh && !meshFound) {
                    if (callback) {
                        child.rotation.y = Math.PI / 2.0; // Rotate model 90 degrees around Y axis
                        meshFound = true;
                        ModelLoader._instance.saveModel(unitType, child);
                        callback(child);
                    }
                }
            });
        }, function(xhr) {
            if (xhr.total) {
                const percent = xhr.loaded / xhr.total * 100.0;
                console.log("Model loading progress: " + percent + "%");
            }
        }, function(err) {
            console.error("Error while loading model:", err);
        });
    }

    /**
     * Get loaded model for particular unit type
     * @param unitType
     * @returns {*}
     */
    getModelForUnit(unitType) {
        const key = ModelLoader._getModelKey(unitType);
        if (this.models[key] !== undefined) {
            return this.models[key];
        } else {
            return null;
        }
    }

    /**
     * Look for a 3D model mesh for a given unit type, return a clone of it that can be placed anywhere in the world.
     * @param unitType
     * @returns {object|null}
     */
    cloneMeshForUnit(unitType) {
        const model = this.getModelForUnit(unitType);
        if (model) {
            return model.clone();
        } else {
            return null;
        }
    }
}
