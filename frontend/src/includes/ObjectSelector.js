import * as THREE from "three";
import "./GameObject.js"

// TODO - refactor selection - call a callback with selected object, don't handle it directly here

/**
 * Responsible for object selection in the scene
 */
export default class ObjectSelector {
    /**
     * Initialize object selector
     * @param scene {Scene}
     * @param selectionCallback {function} A callback function to call when a new object is selected (or deselected).
     * The function must support two arguments: the selected object and state: true when object selected, false when deselected
     */
    constructor(scene, selectionCallback, highlightCallback) {
        this.scene = scene;
        this.canvas = scene.getCanvas();
        this.raycaster = new THREE.Raycaster();
        this.mouse = new THREE.Vector2();
        this.controlCameraObj = scene.controls.object; // Position of the camera within MapControls
        this.selectionCallback = selectionCallback;
        this.highlightCallback = highlightCallback;

        // Stored camera properties onMouseDown. Used onMouseUp to check if camera position has changed.
        // Click is registered only when camera is not moved (no drag has happened)
        this.downValues = null;

        // Previously intersected game object. Used onMouseMove to not redraw selection all the time.
        /** @type GameObject */
        this.prevObj = null;
        // Previously selected object. Used when clicking.
        this.prevSelectedObj = null;

        // Make "this" available in the methods
        this._trackMouseUp = this._trackMouseUp.bind(this);
        this._trackMouseDown = this._trackMouseDown.bind(this);
        this._trackMouseMove = this._trackMouseMove.bind(this);
        this._mouseOverCallback = this._mouseOverCallback.bind(this);
        this._mouseClickCallback = this._mouseClickCallback.bind(this);
    }

    /**
     * Start tracking of mouse events
     */
    startTracking() {
        window.addEventListener('mouseup', this._trackMouseUp, false);
        window.addEventListener('mousedown', this._trackMouseDown, false);
        window.addEventListener('mousemove', this._trackMouseMove, false);
    }

    /**
     * Stop tracking mouse events
     */
    stopTracking() {
        window.removeEventListener('mouseup', this._trackMouseUp, false);
        window.removeEventListener('mousedown', this._trackMouseDown, false);
        window.removeEventListener('mousemove', this._trackMouseMove, false);
    }

    /**
     * Get currently selected gameObject
     * @returns {GameObject|null}
     */
    getSelectedObj() {
        return this.prevSelectedObj;
    }

    /**
     * Deselect currently selected object, notify listeners
     */
    deselect() {
        if (this.prevSelectedObj) {
            this.prevSelectedObj.deselect();
            if (this.selectionCallback) {
                this.selectionCallback(this.prevSelectedObj, false);
            }
            this.prevSelectedObj = null;
        }
    }

    /**
     * Remove highlight from the currently highlighted object
     */
    removeHighlight() {
        if (this.prevObj) {
            this.prevObj.deselect();
            this.prevObj = null;
        }
    }

    /**
     * Find all objects under the mouse, call callback function for each of them
     * @param e Event that happened (mouse move, click, etc). Contains mouse position
     * @param callbackFunction
     * @private
     */
    _rayCast(e, callbackFunction) {
        // Ignore events when camera position changed (user dragged or rotated camera)
        if (this._cameraDragged()) return;

        //1. sets the mouse position with a coordinate system where the center
        //    of the screen is the origin
        this.mouse.x = ((e.clientX - this.scene.offsetLeft) / this.canvas.width) * 2 - 1;
        this.mouse.y = -((e.clientY - this.scene.offsetTop) / this.canvas.height) * 2 + 1;

        //2. set the picking ray from the camera position and mouse coordinates
        this.raycaster.setFromCamera(this.mouse, this.scene.camera);

        //3. compute intersections
        const intersects = this.raycaster.intersectObjects(this.scene.scene.children);

        for (let i = 0; i < intersects.length; i++) {
            callbackFunction(intersects[i].object);
        }
    }

    /**
     * Check if camera has moved, since last mouseDown event (should be only called while mouse is not released)
     * @return {boolean} true if camera has been moved or rotated since last mouseDown
     * @private
     */
    _cameraDragged() {
        // No down values stored, nothing has changed
        if (!this.downValues) return false;

        // Getting positions stored on mouse down.
        const downPosition = this.downValues.position;
        const downQuaternion = this.downValues.quaternion;

        // Getting current positions.
        const upPosition = this.controlCameraObj.position;
        const upQuaternion = this.controlCameraObj.quaternion;

        const downPositionEquals = (downPosition.x.toPrecision(4) === upPosition.x.toPrecision(4) &&
            downPosition.y.toPrecision(4) === upPosition.y.toPrecision(4) &&
            downPosition.z.toPrecision(4) === upPosition.z.toPrecision(4));

        const downQuaternionEquals = (downQuaternion._x.toPrecision(4) === upQuaternion._x.toPrecision(4) &&
            downQuaternion._y.toPrecision(4) === upQuaternion._y.toPrecision(4) &&
            downQuaternion._z.toPrecision(4) === upQuaternion._z.toPrecision(4) &&
            downQuaternion._w.toPrecision(4) === upQuaternion._w.toPrecision(4));

        return !downPositionEquals || !downQuaternionEquals
    }

    /**
     * Called when user moves the mouse.
     * @param e The event, contains mouse position
     * @private
     */
    _trackMouseMove(e) {
        this._rayCast(e, this._mouseOverCallback);
    }

    /**
     * Called when user releases mouse button.
     * @param e The event, contains mouse position
     * @private
     */
    _trackMouseUp(e) {
        this._rayCast(e, this._mouseClickCallback);
        // Unset previous camera configuration.
        this.downValues = null;
    }

    /**
     * Called when user presses any mouse button.
     * @param e The event, contains mouse position
     * @private
     */
    _trackMouseDown(e) {
        const downPosition = this.controlCameraObj.position;
        const downQuaternion = this.controlCameraObj.quaternion;
        this.downValues = {
            position: downPosition.clone(),
            quaternion: downQuaternion.clone()
        };
    }

    /**
     * Called when mouse clicked on an object
     *
     * @param mesh Mesh object that intersects with current mouse location (mouse clicked on it)
     */
    _mouseClickCallback(mesh) {
        /** @type GameObject */
        const selectedGameObject = this.scene.checkIfExists(mesh.uuid);
        if (!selectedGameObject) {
            return;
        }

        // Allow only single selected object
        if (this.prevSelectedObj && this.prevSelectedObj !== selectedGameObject) {
            this.prevSelectedObj.deselect();
        }

        if (!selectedGameObject.isSelected()) {
            selectedGameObject.select();
            this.prevSelectedObj = selectedGameObject;
            if (this.selectionCallback) {
                // Notify the listener that a new object is selected
                this.selectionCallback(selectedGameObject, true);
            }
        } else {
            selectedGameObject.deselect();
            this.prevObj = selectedGameObject;
            if (this.selectionCallback) {
                // Notify the listener that the object is deselected
                this.selectionCallback(selectedGameObject, false);
            }
        }
    }

    /**
     * Called when mouse moved over an object
     *
     * @param mesh Mesh object that intersects with current mouse location
     */
    _mouseOverCallback(mesh) {
        const intersectedObj = this.scene.checkIfExists(mesh.uuid);

        // Only if moved to another cell.
        if (!intersectedObj || intersectedObj === this.prevObj) {
            return;
        }

        // Only remove the frame from previous cell if it was not selected
        if (this.prevObj && !this.prevObj.isSelected()) {
            this.prevObj.deselect('mouseover');
        }

        intersectedObj.highlight();
        if (this.highlightCallback) {
            this.highlightCallback(intersectedObj);
        }
        this.prevObj = intersectedObj;
    }

}


