import * as THREE from 'three'
import MapControls from '../libs/MapControls'
import RestClient from "../RestClient";
import Cell from "./Cell";
import Unit from "./Unit";
import GameObject from "./GameObject";
import ModelLoader from "./ModelLoader";
import ObjectSelector from "./ObjectSelector";
import {dispatch, errorHappened, getSelectedUnit, subscribeToUpdates} from "../ReduxStore";

let scene = null; // Need this in some cases where "this" is not accessible

const UNIT_STATES = {
    BUILDING: "building",
    READY: "ready"
};

// Opacity of units which are under construction
const BUILDING_OPACITY = 0.5;
// Opacity of units while dragging them over the terrain
const DRAGGING_OPACITY = 0.2;

class Scene {
    constructor(canvas, offsetLeft, offsetTop) {
        scene = this; // Initialize the global scene reference
        // clock = new THREE.Clock();
        this.scene = new THREE.Scene();
        this.sceneSubjects = [];

        this.worldWidthSegments = 199;
        this.worldHeightSegments = 199;

        this.worldWidth = 1990;
        this.worldHeight = 1990;

        this.cellWidth = this.worldWidth / this.worldWidthSegments;
        this.cellHeight = this.worldHeight / this.worldHeightSegments;

        // Bigger number, bigger mountains.
        this.terrainFlattener = 0.2;

        this.canvas = canvas;
        this.renderer = this.buildRender(canvas);
        this.camera = this.buildCamera();
        this.controls = this.buildControls();
        this.buildLight();
        this.offsetLeft = offsetLeft;
        this.offsetTop = offsetTop;

        this.objSelector = new ObjectSelector(this, this.onObjectSelected, this.onObjectHighlighted);
        this.objSelector.startTracking();

        this.selectedCell = null;
        this.selectedType = null;
        this.tempUnitMesh = null; // Unit mesh to be shown while the user is dragging mouse over the terrain

        // All units array.
        this.allUnits = null;

        // Binding needed so that "this" is accessible withing callbacks
        this.onUnitsLoaded = this.onUnitsLoaded.bind(this);
        this.onModelsLoaded = this.onModelsLoaded.bind(this);
        this.onUnitsOnlyLoaded = this.onUnitsOnlyLoaded.bind(this);
        this.onObjectSelected = this.onObjectSelected.bind(this);
        this.onUnitPlaced = this.onUnitPlaced.bind(this);
        this.onObjectHighlighted = this.onObjectHighlighted.bind(this);
        this.onStoreStateChanged = this.onStoreStateChanged.bind(this);

        // Subscribe to Redux store updates
        subscribeToUpdates(this.onStoreStateChanged);
    }

    getCanvas() {
        return this.canvas;
    }

    buildCamera() {
        const camera = new THREE.PerspectiveCamera(60, this.canvas.width / this.canvas.height, 1, 1000);
        camera.position.set(40, 70, -20);

        return camera;
    }

    buildRender(canvas) {
        const renderer = new THREE.WebGLRenderer({
            canvas: canvas,
            antialias: false,
            clearColor: 0x050505,
            clearAlpha: 1,
            alpha: false
        });
        renderer.setSize(canvas.width, canvas.height);

        return renderer;
    }

    buildControls() {
        const controls = new MapControls(this.camera, this.renderer.domElement);
        //controls.addEventListener( 'change', render ); // call this only in static scenes (i.e., if there is no animation loop)
        controls.enableDamping = true; // an animation loop is required when either damping or auto-rotation are enabled
        controls.dampingFactor = 0.25;
        controls.screenSpacePanning = false;
        controls.minDistance = 30;
        controls.maxDistance = 600;
        controls.maxPolarAngle = Math.PI / 2.33;

        return controls;
    }

    buildLight() {
        // add spotlight for the shadows
        let mainLight = new THREE.SpotLight(0xffffff, 4);
        mainLight.position.set(200, 200, 200);
        let backLight = new THREE.PointLight(0xffffff, 1);
        backLight.position.set(-100, 100, -100);
        let fillLight = new THREE.PointLight(0xffffff, 1);
        fillLight.position.set(-100, 100, 100);

        this.scene.add(mainLight);
        this.scene.add(backLight);
        this.scene.add(fillLight);

        const axes = new THREE.AxesHelper(200);
        this.scene.add(axes);

        return mainLight;
    }

    /**
     * Function to add 3D object to internal storage.
     *
     * @param uuid
     * @param subject
     */
    addObject(subject) {
        if (subject instanceof GameObject) {
            const uuid = subject.uuid;
            if (!this.checkIfExists(uuid)) {
                this.sceneSubjects[uuid] = subject;
            }
        }
    }

    /**
     * Function to check if uuid exists in storage and return object if exist.
     *
     * @param uuid
     * @returns {null|GameObject} The object if it exists, false otherwise
     */
    checkIfExists(uuid) {
        const s = this.sceneSubjects[uuid];
        if (s !== undefined) {
            return s;
        } else {
            return null;
        }
    }

    /**
     * Callback method for 3D mode loading.
     */
    onModelsLoaded() {
        // Fetch data from backend when component is ready
        RestClient.getUnitTypes(this.onUnitsOnlyLoaded);

        // Request unit data from the backend
        RestClient.getUnitsAndTerrain(this.onUnitsLoaded);
    }

    /**
     * Callback method for all types loading.
     */
    onUnitsOnlyLoaded(unitData) {
        this.allUnits = unitData;
    }

    /**
     * Function to find unit array by given unit type.
     * @param int unit_type - type of unit to search for.
     * @returns array or null
     * @private
     */
    _findCleanUnitByType(unit_type) {
        for (let i = 0; i < this.allUnits.length; ++i) {
            const u = this.allUnits[i];

            if (u.id) {
                if (unit_type === u.id) {
                    return u;
                }
            }
        }

        return null;
    }
    // This method will be called when unit data arrives from the backend
    onUnitsLoaded(unitData) {
        const units = unitData.units;
        console.log("Units and terrain received from backend");

        for (let i = 0; i < units.length; ++i) {
            const u = units[i];

            const cell = this.drawCell(u);
            if (u.unit_type) {
                let opacity = 1.0;
                if (u.state === UNIT_STATES.BUILDING) {
                    // Show "Building in progress" units transparent
                    opacity = BUILDING_OPACITY;
                }
                const unit = this.drawUnit(u.unit_type, cell, opacity);

                // Set clean unit type array, to use for interacting with unit menu.
                const cleanUnit = this._findCleanUnitByType(u.unit_type);
                unit.typeStorage = cleanUnit;
                unit.cell_info = u;

                cell.setUnit(unit);
            }
        }
    }

    /**
     * Called when a gameObject is highlighted
     * @param gameObj
     */
    onObjectHighlighted(gameObj) {
        const selectedUnitType = getSelectedUnit();
        if (selectedUnitType && gameObj instanceof GameObject) {
            // A unit is selected in the menu and the user moved the mouse over a cell
            /** @type Cell */
            const cell = gameObj;
            const unitType = selectedUnitType.id;

            // P.S. Can't reference "this" here, because that refers to ObjectSelector object!

            if (scene.selectedType !== selectedUnitType && scene.tempUnitMesh) {
                // A unit mesh of a different type exists, remove the old one
                console.log("Different unit selected");
                scene.scene.remove(scene.tempUnitMesh);
                scene.tempUnitMesh = null;
            }

            if (!scene.tempUnitMesh) {
                // Create a transparent 3D model that we will drag along the mouse cursor
                const modelLoader = ModelLoader.getInstance();
                scene.tempUnitMesh = modelLoader.cloneMeshForUnit(unitType);
                gameObj.setMaterialOpacity(scene.tempUnitMesh, DRAGGING_OPACITY);
                scene.scene.add(scene.tempUnitMesh);
                scene.selectedType = selectedUnitType;
            }

            if (cell.isEmpty()) {
                // Move the transparent unit to the cell
                const cellPos = cell.getCenterLocation();
                scene.tempUnitMesh.position.set(cellPos.x, cellPos.y, cellPos.z);
            }
        }
    }

    /**
     * Called when a gameObject is selected or deselected
     * @param gameObj {GameObject}
     * @param selected {boolean} True when object selected, false when deselected
     */
    onObjectSelected(gameObj, selected) {
        const selectedUnitType = getSelectedUnit();
        if (selectedUnitType && selected && gameObj instanceof GameObject) {
            const unitTypeId = selectedUnitType.id;
            const row = gameObj.rowNumber;
            const col = gameObj.colNumber;

            // Will need to reference the cell and unit type when response arrives from backend
            // P.S. Can't reference "this" here, because that refers to ObjectSelector object!
            scene.selectedCell = gameObj;
            scene.selectedType = selectedUnitType;

            // Remove the temporary "dragging" unit
            if (this.tempUnitMesh) {
                this.scene.remove(this.tempUnitMesh);
                this.tempUnitMesh = null;
            }

            RestClient.placeUnit(row, col, unitTypeId, scene.onUnitPlaced);
        }
    }

    /**
     * This method is called when response to placeUnit REST request comes from the backend
     * @param response {object}
     */
    onUnitPlaced(response) {
        console.log("unit place response:");
        console.log(response);
        if (response.ok) {
            // Unit placed successfully
            if (this.selectedCell) {
                this.drawUnit(this.selectedType.id, this.selectedCell, BUILDING_OPACITY);
            }
        } else {
            // Unit not placed. Deselect the cell, show error
            if (response.err) {
                dispatch(errorHappened(response.err));
            }
        }
        this.objSelector.deselect();
        this.selectedCell = null;
    }

    /**
     * Draw one cell of terrain
     * @param loc Location info, containing row, col, and directions: nw, ne, sw, se (Northwest, etc)
     */
    drawCell(loc) {
        let cell = new Cell(this, loc.row, loc.col);

        // The naming of variables is "turned 90 degrees", therefore they don't
        // really correspond correctly with North, South, West and East.

        // X coords
        const xTop = this.cellHeight * loc.col;
        const xBottom = xTop + this.cellHeight;

        // Z coords
        const zRight = this.cellWidth * loc.row;
        const zLeft = zRight + this.cellWidth;

        // Y coords
        const yTopRight = loc.nw * this.terrainFlattener;
        const yBottomRight = loc.ne * this.terrainFlattener;
        const yTopLeft = loc.sw * this.terrainFlattener;
        const yBottomLeft = loc.se * this.terrainFlattener;

        let coordArray = [
            xTop, yTopRight, zRight,
            xBottom, yBottomRight, zRight,
            xTop, yTopLeft, zLeft,
            xBottom, yBottomLeft, zLeft
        ];

        cell.draw(this.cellWidth, this.cellHeight, coordArray);
        this.addObject(cell);
        return cell;
    }

    /**
     * Place a unit of specific type on a terrain cell.
     * @param unitType
     * @param cell
     * @param opacity Opacity of the unit. When < 1.0, the object will be transparent
     */
    drawUnit(unitType, cell, opacity) {
        let unit = new Unit(this);

        unit.draw(unitType, cell, opacity);
        this.addObject(unit);

        return unit;
    }

    /**
     * Render the next frame. Consider how much time spent since last update.
     */
    update() {
        this.controls.update();
        /*const elapsedTime = this.clock.getElapsedTime();
        for (let i = 0; i < this.sceneSubjects.length; i++) {
            this.sceneSubjects[i].update(elapsedTime);
        }*/
        this.renderer.render(this.scene, this.camera);
    }

    /**
     * This function is called whenever Redux store state changes
     */
    onStoreStateChanged() {
        const st = getSelectedUnit();
        if (this.selectedType !== st) {
            // When another icon selected in the menu, clear current unit selection
            this.selectedType = st;
            if (this.tempUnitMesh) {
                this.scene.remove(this.tempUnitMesh);
                this.tempUnitMesh = null;
            }
            // Deselect the last highlighted cell
            this.objSelector.removeHighlight();
        }
    }
}

export default Scene;