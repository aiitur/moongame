import TerrainLoader from '../libs/TerrainLoader'
import Cell from "./Cell"

// Load bin file for terrain.
import tempTerrain from "../assets/temp_terrain.bin"

// Global variable for scene object.
let sceneLoader = null;

class Terrain {
    constructor(scene) {
        sceneLoader = scene;
    }

    load() {
        const terrainLoader = new TerrainLoader();
        terrainLoader.load(tempTerrain, function (data) {
            // Size of the terrain: how many cells wide and high it is
            const worldWidthSegments = sceneLoader.worldWidthSegments;
            const worldHeightSegments = sceneLoader.worldHeightSegments;

            const cellWidth = sceneLoader.cellWidth;
            const cellHeight = sceneLoader.cellHeight;
            const worldWidth = sceneLoader.worldWidth;

            for (let all = 0; all < worldWidthSegments * 100; all++) {
                const rowNumber = Math.floor(all / worldWidthSegments);
                const cellNumber = all % worldWidthSegments;

                const lastXTopRightIndex = rowNumber * worldWidthSegments + cellNumber;
                const lastXTopLeftIndex = lastXTopRightIndex + worldWidthSegments;

                // X coords
                const xTop = -worldWidth / 2 + (cellHeight * cellNumber);
                const xBottom = xTop + cellHeight;

                // Z coords
                const zRight = -worldWidth / 2 + (cellWidth * rowNumber);
                const zLeft = zRight + cellWidth;

                // Y coords
                const yTopRight = data[lastXTopRightIndex] / 65535 * sceneLoader.terrainFlattener;
                const yBottomRight = data[lastXTopRightIndex + 1] / 65535 * sceneLoader.terrainFlattener;
                const yTopLeft = data[lastXTopLeftIndex] / 65535 * sceneLoader.terrainFlattener;
                const yBottomLeft = data[lastXTopLeftIndex + 1] / 65535 * sceneLoader.terrainFlattener;

                let coordArray = [];

                coordArray[0] = xTop; // X axis coord
                coordArray[1] = yTopRight; // Y axis coord
                coordArray[2] = zRight; // Z axis coord

                coordArray[3] = xBottom; // X axis coord
                coordArray[4] = yBottomRight; // Y axis coord
                coordArray[5] = zRight; // Z axis coord

                coordArray[6] = xTop; // X axis coord
                coordArray[7] = yTopLeft; // Y axis coord
                coordArray[8] = zLeft; // Z axis coord

                coordArray[9] = xBottom; // X axis coord
                coordArray[10] = yBottomLeft; // Y axis coord
                coordArray[11] = zLeft; // Z axis coord

                let cell = new Cell(sceneLoader, rowNumber, cellNumber);
                cell.draw(cellWidth, cellHeight, coordArray);

                // Storing cell object internally.
                sceneLoader.addObject(cell);
            }
        });
    }
}

export default Terrain;