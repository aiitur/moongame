import Scene from "./Scene"
import ModelLoader from "./ModelLoader";

let sceneLoader = null;

// TODO - Ideas for refactor:
// *) Rename sceneLoader to Scene. It does not load scenes, it represents a single scene

class ThreeEntryPoint {
    constructor(containerElement, canvasWidth, canvasHeight, offsetLeft, offsetTop) {
        // This code will be run once someone calls threeEntryPoint(containerElement).
        this.canvas = this.createCanvas(containerElement);
        this.canvas.width = canvasWidth;
        this.canvas.height = canvasHeight;
        sceneLoader = new Scene(this.canvas, offsetLeft, offsetTop);

        // Load the 3D models, notify Scene when done
        const modelLoader = ModelLoader.getInstance();
        modelLoader.load(sceneLoader.onModelsLoaded);

        // const terrainLoader = new Terrain(sceneLoader);
        // terrainLoader.load();

        render();
    }

    // Create a canvas element and append it as a child to the containerElement.
    createCanvas(containerElement) {
        const canvas = document.createElement("canvas");
        containerElement.appendChild(canvas);
        return canvas;
    }
}

// Render the three.js world.
function render() {
    requestAnimationFrame(render);
    sceneLoader.update();
}

export default ThreeEntryPoint;
