import GameObject from "./GameObject";
import ModelLoader from "./ModelLoader";
import {dispatch} from "../ReduxStore.js"
import {unitShowInfo} from "../ReduxStore";

// Global variable for scene object.
let sceneLoader = null;

class Unit extends GameObject {
    constructor(scene) {
        super();
        sceneLoader = scene;
        this.uuid = null;
        this.parentCell = null;
        this.type = null;
        this.typeStorage = null;
        this.cell_info = null;
    }

    /**
     * Place a unit of specific type on a terrain cell
     * @param unitType
     * @param cell
     * @param opacity Opacity of the unit. When < 1.0, the object will be transparent
     */
    draw(unitType, cell, opacity = 1.0) {
        const modelLoader = ModelLoader.getInstance();
        let unitMesh = modelLoader.cloneMeshForUnit(unitType);

        // Setting class uuid.
        if (unitMesh) {
            this.uuid = unitMesh.uuid;
        }
        this.parentCell = cell;
        this.type = unitType;

        if (unitMesh) {
            if (opacity < 1.0) {
                this.setMaterialOpacity(unitMesh, opacity);
            }
            const cellPos = cell.getCenterLocation();
            unitMesh.position.set(cellPos.x, cellPos.y, cellPos.z);
            sceneLoader.scene.add(unitMesh);
        } else {
            console.error(`Could not find 3D model for unit with type ${unitType}!`);
        }
    }

    /**
     * Function to draw/process unit selection square.
     * @param withoutSelection - if need just to set state, not draw.
     */
    drawSelection(withoutSelection=false) {
        // Find parent cell and select that.
        if (!withoutSelection) {
            this.parentCell.drawSelection(true);
        }

        // TODO: this will be loaded from frontend in future versions.
        this.typeStorage.cell_info = this.cell_info;

        // Display only for Radars for now.
        if (this.type === 2) {
            this.typeStorage.status_switcher = true;
        }

        // Dispat ch an action to Redux store. It will notify all listeners
        dispatch(unitShowInfo(this.typeStorage, true));
    }

    isEmpty() {
        return false;
    }

    /**
     * Function to draw unit highlight square.
     */
    drawHighlight() {
        // Leave empty, highlight functionality takes cell for now.
    }

    /**
     * Function to remove unit selection and free objects.
     */
    removeHighlight(eventName) {
        if (eventName === 'click') {
            // Dispat ch an action to Redux store. It will notify all listeners
            dispatch(unitShowInfo(this.typeStorage, false));
        }
    }
}

export default Unit;
