import React from 'react';
import * as ReactDOM from "react-dom";
import "./index.css"
import ThreeContainer from "./components/ThreeContainer";
import MenuContainer from "./components/MenuContainer";

const MENU_WIDTH = 200;
// Offset for ThreeJS canvas
const OFFSET_TOP = 0;

// The main React component for the whole app
class App extends React.Component {
    render() {
        return (
            <div id="app-container">
                <MenuContainer width={MENU_WIDTH} />
                <ThreeContainer offsetLeft={MENU_WIDTH} offsetTop={OFFSET_TOP}/>
            </div>
        );
    }
}

ReactDOM.render(
    <App/>,
    document.getElementById("root")
);