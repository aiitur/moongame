<?php

// Platform-specific configuration: platform.config.php
// This file should NOT be in the GIT. Instead, an example platform-config-template.php should be in the git
// Devs should take the template, fill in the local parameters
// Whenever someone updates any config parameters, the template file must be updated as well!

define("DB_HOST", "localhost");
define("DB_NAME", "moon");
define("DB_USER", "moon");
define("DB_PWD", "your-pwd-here");
