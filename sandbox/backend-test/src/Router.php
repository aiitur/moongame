<?php

namespace Moon;

use AltoRouter;

/**
 * Takes URL path, finds necessary controller object method or plain function and calls it.
 * Uses AltoRouter component underneath.
 */
class Router {
  private $alto_router;

  public function __construct() {
    $this->alto_router = new AltoRouter();
    $this->alto_router->setBasePath('/backend');
    $this->registerRoutes();
  }

  /**
   * Consider the path of current HTTP request, find a matching route and call its handle.
   * @return bool True when route found and processed correctly, false otherwise
   */
  public function process() {
    $route_processed = false;
    $match = $this->alto_router->match();
    if ($match) {
      $route_processed = $this->processRoute($match);
    }
    return $route_processed;
  }

  /**
   * Register all the route mappings
   */
  private function registerRoutes() {
    try {
      $this->alto_router->map("POST", "/units/place", "Moon\UnitController#place", "place_unit");
      $this->alto_router->map("GET", "/units/get_all", "Moon\UnitController#getAll", "get_all_units");
      $this->alto_router->map("GET", "/units/delete/[i:row]/[i:col]", "Moon\UnitController#delete", "delete_unit");
    } catch (\Exception $e) {
      debug_log("Error while registering route: " . $e->getMessage());
    }
  }

  /**
   * Take an AltoRouter match, and call necessary handler function
   * @param array $match Returned by AltoRouter->match()
   * @return bool True when route successfully processed, false otherwise
   */
  function processRoute($match) {
    if (!$match) {
      debug_log("No match for route");
      return false;
    }

    $response = null;
    $class_name = null;
    $method_name = null;
    $t = $match["target"];
    $p = $match["params"];

    if (is_callable($t)) {
      // Simple function - call it directly
      $response = call_user_func_array($t, $p);
    } else if (is_array($t) && isset($t['c']) && isset($t['a'])) {
      $class_name = $t['c'];
      $method_name = $t['a'];
    } else {
      // target = class#method
      $parts = explode("#", $t);
      if (count($parts) == 2) {
        $class_name = $parts[0];
        $method_name = $parts[1];
      }
    }

    if ($class_name && $method_name) {
      if (class_exists($class_name)) {
        //debug_log("Class $class_name exists<br/>");
        $obj = new $class_name();
        if (method_exists($obj, $method_name)) {
          //debug_log("Method $class_name::$method_name exists<br/>");
          // Extract parameters from GET and POST data
          if (isset($match["params"]) && $match["params"]) {
            $url_params = $match["params"];
          } else {
            $url_params = null;
          }
          $post_data = file_get_contents('php://input');
          $response = $obj->$method_name($url_params, $post_data);
        }
      }
    }

    // TODO - will probably need a way how controller can return a Response object, including the status code?
    if (!$response) {
      //debug_log("Could not call the matching function! <br/>");
    }

    return $response;
  }

}