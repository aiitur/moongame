<?php

namespace Moon;


class Unit {
  /** @var int Row in the grid where the unit is placed */
  public $row;
  /** @var int Column in the grid where the unit is placed */
  public $col;
  /** @var string Type of the unit */
  public $unit;

  public function __construct($row, $col, $unit) {
    $this->row = $row;
    $this->col = $col;
    $this->unit = $unit;
  }
}