<?php

namespace Moon;


class UnitController {
  public function place($params, $post_data) {
    $data = json_decode($post_data);
    try {
      if (isset($data->row) && isset($data->col) && isset($data->unit)) {
        $repo = new UnitRepository();
        if ($repo->placeUnit($data->row, $data->col, $data->unit)) {
          return "\"Successfully placed\"";
        } else {
          header("HTTP/1.1 400 Unit placement failed");
        }
      }
    } catch (\Exception $e) {
      debug_log("Wrong POST data: " . $e->getMessage());
    }
    return "\"Unit placement failed\"";
  }

  /**
   * Get all units
   */
  public function getAll() {
    $repo = new UnitRepository();
    return json_encode($repo->getAll());
  }

  /**
   * Delete a unit
   * @param $params
   * @return int number of units deleted, 0 on error
   */
  public function delete($params) {
    if (!isset($params["row"]) || !isset($params["col"])) {
      return 0;
    }

    $repo = new UnitRepository();
    return $repo->delete($params["row"], $params["col"]);
  }
}