<?php

namespace Moon;

class UnitRepository {

  /**
   * Try to place a unit at given row and column in the DB.
   * @param $row
   * @param $col
   * @param $unit
   * @return int 1 on success, 0 otherwise
   */
  public function placeUnit($row, $col, $unit) {
    $db = DbConn::getInstance();
    if (!$db) return false;

    // Try to insert the value - if it fails, then a unit is already placed there (unique constraint raises exception)
    return $db->execUpdateQuery("INSERT INTO units (row, col, unit) VALUES (?, ?, ?)", [$row, $col, $unit]);
  }

  /**
   * @return array List of all units or empty array on error
   */
  public function getAll() {
    $db = DbConn::getInstance();
    if (!$db) return [];

    $units = [];
    // Try to insert the value - if it fails, then a unit is already placed there (unique constraint raises exception)
    $results = $db->execSelectQuery("SELECT row, col, unit FROM units ORDER BY row ASC, col ASC", []);
    if ($results) {
      foreach ($results as $item) {
        $units[] = new Unit($item["row"], $item["col"], $item["unit"]);
      }
    }
    return $units;
  }

  /**
   * Delete a unit placed in specific row and column
   * @param $row
   * @param $col
   * @return int number of units deleted. 0 on error
   */
  public function delete($row, $col) {
    $db = DbConn::getInstance();
    if (!$db) return 0;

    return $db->execUpdateQuery("DELETE FROM units WHERE row = ? AND col = ?", [$row, $col]);

  }
}