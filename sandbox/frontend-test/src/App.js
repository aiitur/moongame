import React, {Component} from 'react';
import './App.css';
import {BrowserRouter as Router, Route} from "react-router-dom";
import Stats from "./components/Stats";
import GameView from "./components/GameView";
import Header from "./components/Header";

class App extends Component {
  render() {
    return (
      <Router>
        <div className="main_container">
          <Header/>
          <div className="content_area">
            <Route exact path="/" component={GameView}/>
            <Route path="/stats" component={Stats}/>
          </div>
        </div>
      </Router>
    );
  }
}

export default App;
