import house from "./images/menu_items/house.png";
import tank from "./images/menu_items/tank.png";
import del_icon from "./images/icons/delete.png";

// A mapping from unit name to icon image resource
export const unit_images = {
  "house": house,
  "tank": tank
};

// A mapping from icon to image resource
export const icon_images = {
  "delete": del_icon
};

/**
 * Find image resource corresponding to a unit, return its URL, or null on error
 * @param unit_name
 * @returns {*}
 */
export function findUnitIcon(unit_name) {
  if (unit_images[unit_name]) {
    return unit_images[unit_name];
  } else {
    return null;
  }
}

/**
 * Find generic image resource, not bound to a unit, return its URL, or null on error
 * @param icon_name
 * @returns {*}
 */
export function findIcon(icon_name) {
  if (icon_images[icon_name]) {
    return icon_images[icon_name];
  } else {
    return null;
  }
}

