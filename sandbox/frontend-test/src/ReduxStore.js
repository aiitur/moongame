import * as Redux from 'redux';

const ACTIONS = {
  UNIT_SELECTED: "UNIT_SELECTED",
};

export function unitSelected(unit, is_selected) {
  if (!is_selected) {
    unit = null;
  }
  return {
    type: ACTIONS.UNIT_SELECTED,
    unit: unit,
  }
}

const EMPTY_STATE = {
  selected_unit: null
};

// Shallow copy of the object
// Taken from https://medium.com/@Farzad_YZ/3-ways-to-clone-objects-in-javascript-f752d148054d
function shallowCopy(src) {
  let target = {};
  for (let prop in src) {
    if (src.hasOwnProperty(prop)) {
      target[prop] = src[prop];
    }
  }
  return target;
}

function reducer(state = EMPTY_STATE, action) {
  let new_state = shallowCopy(state);
  switch (action.type) {
    case ACTIONS.UNIT_SELECTED:
      new_state["selected_unit"] = {unit: action.unit};
      break;
    default:
      break;
  }
  console.log("new_state:");
  console.log(new_state);
  return new_state;
}


var redux_store = Redux.createStore(reducer);
console.log("Store created");

export function dispatch(action) {
  console.log("dispatch");
  console.log(action);
  redux_store.dispatch(action);
}

export function getStoreState() {
  return redux_store.getState();
}

export function subscribeToUpdates(event_callback) {
  redux_store.subscribe(event_callback);
}