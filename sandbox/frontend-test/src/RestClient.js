// This file contains all the REST API calls

// Get the backend API URL from configuration
const API_URL_BASE = process.env.REACT_APP_BACKEND_URL;

/**
 * Send request to the backend, try to place unit in given cell
 * Call callback function when response from backend comes: the parameter will be the response from the server
 * Use response.ok to check whether the response was successful
 *
 * @param row_index Index of row where to place the unit
 * @param col_index Index of column where to place the unit
 * @param unit
 * @param callback Function that will be called when response is received
 */
export function placeUnit(row_index, col_index, unit, callback) {
  const api_url = API_URL_BASE + "/units/place";
  const data = {row: row_index, col: col_index, unit: unit};
  fetch(api_url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(data)
  }).then(callback);
}

/**
 * Send request to get all currently placed units
 * @param callback Function that will be called when response is received
 */
export function loadPlacedUnits(callback) {
  const api_url = API_URL_BASE + "/units/get_all";
  fetch(api_url).then(response => {
    return response.json();
  }).then(callback);
}

/**
 * Send request to the backend, try to delete unit in given cell
 * Call callback function when response from backend comes: the parameter will be the response from the server
 * It should contain one number: number of units removed (1 on success, 0 on error)
 *
 * @param row_index Index of row where to delete the unit
 * @param col_index Index of column where to delete the unit
 * @param callback Function that will be called when response is received
 */
export function deleteUnit(row_index, col_index, callback) {
  const api_url = API_URL_BASE + "/units/delete/" + row_index + "/" + col_index;
  fetch(api_url).then(callback);
}
