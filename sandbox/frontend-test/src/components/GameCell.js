import React from 'react';
import {getStoreState} from "../ReduxStore"
import * as REST from "../RestClient";
import {findUnitIcon, findIcon} from "../ImageMap";

export default class GameCell extends React.Component {
  constructor(props) {
    super(props);
    this.state = {unit: this.props.unit, approved: this.props.approved};
    // Bind to get access to 'this' within the methods
    this.placeUnit = this.placeUnit.bind(this);
    this.onMouseEnter = this.onMouseEnter.bind(this);
    this.onMouseLeave = this.onMouseLeave.bind(this);
    this.onUnitPlaced = this.onUnitPlaced.bind(this);
    this.onUnitDeleted = this.onUnitDeleted.bind(this);
    this.deleteUnit = this.deleteUnit.bind(this);
  }

  render() {
    let cellId = "cell_" + this.props.row_index + "_" + this.props.col_index;
    this.style = {
      backgroundImage: null,
      opacity: 1.0,
      backgroundSize: "100% 100%"
    };
    let del_icon = null;
    if (this.state && this.state.unit) {
      // A unit is placed in this cell, must show it
      console.log("A unit is placed in cell " + cellId + ", must show it");
      const selected_img = findUnitIcon(this.state.unit);
      if (selected_img) {
        this.style["backgroundImage"] = "url(" + selected_img + ")";
        let opacity = 0.5;
        if (this.state.approved) {
          // Unit is placed and approved
          opacity = 1.0;
          // Show "Delete" icon for the item
          del_icon = <img src={findIcon("delete")} alt="Delete unit" className="delete" title="Delete unit"
                          onClick={this.deleteUnit}/>;
        }
        this.style.opacity = opacity;
      }
    }
    return <td ref={e => this._td = e} key={cellId} id={cellId} style={this.style} onClick={this.placeUnit}
               onMouseEnter={this.onMouseEnter} onMouseLeave={this.onMouseLeave}>{del_icon}</td>;
  }

  placeUnit(e) {
    // Not allowed to click if a unit is already placed in this cell
    if (this.state && this.state.unit) return;

    console.log("Cell clicked " + this._td.id);
    const store_state = getStoreState();
    if (!store_state.selected_unit) return; // Nothing selected in the menu, don't show anything in the grid
    // Update state for this cell, force render()
    const selected_unit = store_state.selected_unit.unit;
    // Send request to backend
    REST.placeUnit(this.props.row_index, this.props.col_index, selected_unit, this.onUnitPlaced);
    this.setState({unit: selected_unit});
  }

  deleteUnit() {
    this.setState({unit: this.state.unit, approved: false});
    REST.deleteUnit(this.props.row_index, this.props.col_index, this.onUnitDeleted);
  }

  onMouseEnter(e) {
    // Not allowed to place a new unit here if a unit is already placed in this cell
    if (this.state && this.state.unit) return;

    //console.log("Cell clicked, row = " + this._td.id);
    const store_state = getStoreState();
    if (!store_state.selected_unit) return; // Nothing selected in the menu, don't show anything in the grid

    const selected_img = findUnitIcon(store_state.selected_unit.unit);
    if (selected_img) {
      let cell = this._td;
      cell.style.backgroundImage = "url(" + selected_img + ")";
      cell.style.backgroundSize = "100% 100%";
      cell.style.opacity = 0.2;
    }
  }

  onMouseLeave(e) {
    if (this.style) {
      let cell = this._td;
      // Apply the default style again
      for (let prop in this.style) {
        if (this.style.hasOwnProperty(prop)) {
          cell.style[prop] = this.style[prop];
        }
      }
    }
  }

  /**
   * This method is called when backend returns response to "placeUnit" call
   * @param response Backend's response
   */
  onUnitPlaced(response) {
    console.log("Response from backend");
    if (response) {
      let state = this.state;
      if (response.ok) {
        // Placement successful, keep the unit on the cell, change opacity
        state["approved"] = true;
        console.log("Unit approved");
      } else {
        state["approved"] = false;
        state["unit"] = null;
      }
      this.setState(state);
    }
  }


  /**
   * This method is called when backend returns response to "deleteUnit" call
   * @param response Backend's response
   */
  onUnitDeleted(response) {
    if (response) {
      // Unit removed
      console.log("Unit removed");
      this.setState({unit: null, approved: false});
    } else {
      console.log("Remove failed:");
      console.log(response);
    }
  }
}
