import React from 'react';
import GameCell from "./GameCell";
import * as REST from "../RestClient";

export default class GameGrid extends React.Component {
  constructor(props) {
    super(props);
    this.state = {data_loaded: false, units: [], load_failed: false};
    // To get access to "this" in the method
    this.onUnitsReceived = this.onUnitsReceived.bind(this);
  }

  componentDidMount() {
    // When component loaded, send REST request to get currently placed units
    REST.loadPlacedUnits(this.onUnitsReceived);
  }

  render() {
    // Show loading when no data is loaded
    let msg = this.state.load_failed ? "Data load failed, refresh the page!" : "Loading data...";
    let rows = <tr>
      <td className="game_grid_loading">{msg}</td>
    </tr>;

    console.log("Rendering grid...");

    if (this.state.data_loaded) {
      // Data is loaded, show all the rows
      console.log("Rendering grid with data...");
      rows = [];
      for (var row_index = 0; row_index < this.props.rowCount; row_index++) {
        let cells = [];
        for (var col_index = 0; col_index < this.props.columnCount; col_index++) {
          const idx = row_index + "_" + col_index;
          let cell_id = "cell_" + idx;
          const placed_unit = this.state.units[idx];
          let approved = false; // Whether unit is approved by the server
          if (placed_unit) {
            console.log(placed_unit + " placed at " + row_index + ", " + col_index);
            approved = true;
          }
          cells.push(<GameCell key={cell_id} row_index={row_index} col_index={col_index} unit={placed_unit}
                               approved={approved}/>);
        }
        let row_id = "row" + row_index;
        rows.push(<tr key={row_id} id={row_id}>{cells}</tr>);
      }
      console.log(rows.length + " rows generated");
    }
    return (
      <table className="game_grid">
        <tbody>
        {rows}
        </tbody>
      </table>
    )
  }

  onUnitsReceived(response) {
    if (response && response.units) {
      const unit_map = GameGrid.convertUnitArrayToMap(response.units);
      console.log("Units loaded ok");
      this.setState({data_loaded: true, units: unit_map});
    } else {
      this.setState({data_loaded: true, units: [], load_failed: true});
    }
  }

  // Take in array of Unit objects, convert it to a map where key is "<row>_<col>" and value is <unit>
  // Will be easier to check for each cell whether a unit is placed there
  static convertUnitArrayToMap(unit_array) {
    let unit_map = {};
    for (let i = 0; i < unit_array.length; ++i) {
      const u = unit_array[i];
      const idx = u.row + "_" + u.col;
      unit_map[idx] = u.unit;
    }
    return unit_map;
  }
}
