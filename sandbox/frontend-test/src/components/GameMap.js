import React from 'react';
import GameGrid from "./GameGrid";

const NUM_ROWS = 6;
const NUM_COLUMNS = 8;

class GameMap extends React.Component {
  render() {
    return (
      <div className="game_map">
        <GameGrid rowCount={NUM_ROWS} columnCount={NUM_COLUMNS}/>
      </div>
    );
  }
}

export default GameMap;