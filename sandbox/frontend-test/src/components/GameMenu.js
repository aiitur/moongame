import React from 'react';
import GameMenuItem from "./GameMenuItem";
import {getStoreState, subscribeToUpdates} from '../ReduxStore'
import {unit_images} from "../ImageMap";

export default class GameMenu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {selected_unit: null};
    // Need to manually bind component methods so that we can access 'this' within them
    this.storeStateUpdated = this.storeStateUpdated.bind(this);
  }

  componentDidMount() {
    // Subscribe to Redux store notifications about state changes
    subscribeToUpdates(this.storeStateUpdated);
    this.storeStateUpdated();
  }

  render() {
    var icon_items = [];
    for (var unit_name in unit_images) {
      if (unit_images.hasOwnProperty(unit_name)) {
        var is_selected = unit_name === this.state.selected_unit;
        if (is_selected) {
          console.log("Marking icon for " + unit_name + " as selected");
        }
        icon_items.push(<GameMenuItem item={unit_name} key={"gmi_" + unit_name} selected={is_selected}/>);
      }
    }
    return (
      <div className="game_menu">
        {icon_items}
      </div>
    );
  }

  // This method is called whenever we get a notification from Redux that store state is updated
  storeStateUpdated() {
    var store_state = getStoreState();
    var selected_unit = store_state.selected_unit ? store_state.selected_unit.unit : null;
    var new_state = {selected_unit: selected_unit};
    // Setting a new state will force render() method to be called
    this.setState(new_state);
  }
}
