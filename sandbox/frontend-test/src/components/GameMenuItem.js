import React from 'react';
import {dispatch, unitSelected} from '../ReduxStore';
import {findUnitIcon} from "../ImageMap";


class GameMenuItem extends React.Component {
  constructor(props) {
    super(props);
    // Need to manually bind component methods so that we can access 'this' within them
    this.itemClicked = this.itemClicked.bind(this);
  }

  render() {
    const icon_name = this.props.item;
    const icon_path = this.getIconPath();
    var img_node = null;
    if (icon_path) {
      img_node = <img src={icon_path} alt={icon_name} className="menu_item_icon"/>
    }
    var sel_class = this.props.selected ? "selected" : ""; // Additional CSS class, when item is selected
    return (
      <div className={"game_menu_item " + sel_class} onClick={this.itemClicked}>{img_node}</div>
    );
  }

  itemClicked(e) {
    const selected = !this.props.selected; // Toggle selection
    dispatch(unitSelected(this.props.item, selected, this.getIconPath()));
  }

  getIconPath() {
    return findUnitIcon(this.props.item);
  }

}

export default GameMenuItem;