import React from 'react';
import "./GameView.css"
import GameMap from "./GameMap";
import GameMenu from "./GameMenu";

const GameView = () => {
  return (
    <div className="game_view">
      <GameMap/>
      <GameMenu/>
    </div>
  );
};

export default GameView;
