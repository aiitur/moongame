import React, {Component} from 'react';
import './Header.css';
import {Link} from "react-router-dom";

class App extends Component {
  render() {
    return (
      <div className="menu">
        <div className="menu_item">
          <Link to="/">Game</Link>
        </div>
        <div className="menu_item">
          <Link to="/stats">Stats</Link>
        </div>
      </div>
    );
  }
}

export default App;
