// Loads GLTF models
// WARNING: There must be one Parent mesh for the model! Otherwise, the loading won't work correctly!

function loadModelMesh(modelName, callback) {
  const path = 'models/' + modelName + "/";
  const filename = modelName + '.gltf';
  const loader = new THREE.GLTFLoader().setPath(path);
  loader.load(filename, function (gltf) {
    let meshFound = false; // Load the first mesh only
    gltf.scene.traverse(function (child) {
      if (child.isMesh && !meshFound) {
        if (callback) {
          meshFound = true;
          callback(child);
        }
      }
    });
  }, onLoadProgress, onLoadFailed);
}

function onLoadFailed(err) {
  console.error(err);
}

// This function is load sometimes while model is loading
function onLoadProgress(xhr) {
  //const percent = xhr.loaded / xhr.total * 100.0;
  //console.log("Model loading progress: " + percent + "%");
}
