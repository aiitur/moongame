const MySQL = require('mysql');
const utils = require('./utils.js');
const Promise = require('bluebird');

let dbConnOk = false;
let dbConn = null;
let db = null; // Promisified db connection, supports .then() syntax

module.exports = {
  initDbConn: initDbConn,
  executeQuery: executeQuery
};

/**
 * Initiate connection to DB
 * The connection is made in another thread in asynchronous manner. We have to wait for the flag to become true
 * @returns {Promise<void>} Every async function returns a Promise by default
 */
async function initDbConn() {
  connectToDb();
  while (!dbConnOk) {
    utils.log("Waiting for DB connection...");
    await utils.sleep(1000);
  }
}

function connectToDb() {
  try {
    dbConn = MySQL.createConnection({
      host: "localhost",
      user: "test-user",
      password: "test-pwd",
      database: "graphql_test_db"
    });
    db  = Promise.promisifyAll(dbConn);
    dbConn.connect(function(err) {
      if (err) {
        utils.log("Failed to connect to database: " + err);
        process.exit();
      } else {
        dbConnOk = true;
        utils.log("DB connection established");
      }
    });
    utils.log("DB conn scheduled...");
    return true;
  } catch (e) {
    return false;
  }
}

/**
 * Execute a query, wait for response
 * @param query
 * @param values
 * @returns {Promise<Array>|null} Array with results for SELECT queries, OkPacket for INSERT, etc. null on error
 */
async function executeQuery(query, values) {
  let results = [];
  if (dbConnOk) {
    // Wait until response arrives
    console.log("Running query " + query);
    await db.queryAsync({sql: query, values: values}).then(function(rows) {
      results = rows;
    }).catch(function(e) {
      console.log("query failed: " + e.message);
      results = null;
    });
  }
  return results;
}
