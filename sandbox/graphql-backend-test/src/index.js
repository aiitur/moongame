// import does not work with node.js
const {GraphQLServer} = require('graphql-yoga');
const resolvers = require('./resolvers.js');
const DbConn = require('./DbConn.js');
const utils = require('./utils.js');

////////////////////////////////////////
// Launch the main stuff
////////////////////////////////////////
DbConn.initDbConn();
launchGraphqlServer("src/schema.graphql", resolvers);

////////////////////////////////////////


function launchGraphqlServer(schema_path, resolvers) {
// Create and launch GraphQL server
  const server = new GraphQLServer({typeDefs: schema_path, resolvers: resolvers});
  server.start(function() {
    utils.log("Server running at http://localhost:4000");
  });
}
