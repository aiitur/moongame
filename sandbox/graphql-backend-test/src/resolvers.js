const DbConn = require("./DbConn");

// Define resolvers
const resolvers = {
  Query: {
    info: function() {
      return "Hello from GraphQL!";
    },
    messages: async function() {
      const query = "SELECT msg FROM messages";
      const result = await DbConn.executeQuery(query, []);
      console.log("Result in resolver:");
      console.log(result);
      if (!result) return [];
      return result.map(x => x.msg);
    }
  },
  Mutation: {
    createTable: async function(parent, args) {
      const query = "CREATE TABLE `graphql_test_db`.`messages` ( `id` INT NOT NULL AUTO_INCREMENT , "
        + "`msg` NVARCHAR(255) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;";
      const result = await DbConn.executeQuery(query, []);
      console.log("Result in resolver:");
      console.log(result);
      return result != null;
    },
    addMsg: async function(parent, args) {
      const message = args.msg;
      if (message === undefined) return -1;

      const query = "INSERT INTO `messages` (msg) VALUES (?)";
      const result = await DbConn.executeQuery(query, [message]);
      console.log("Result in resolver:");
      console.log(result);
      let newMessageId = -1;
      if (result && result.insertId) {
        newMessageId = result.insertId;
      }
      return newMessageId;
    }
  }
};


module.exports = resolvers;
