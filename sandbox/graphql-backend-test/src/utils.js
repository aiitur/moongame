// Utility module

module.exports = {
  sleep: sleep,
  log: log
};

async function sleep(ms) {
  return new Promise(resolve => {
    setTimeout(resolve, ms);
  })
}

function log(msg) {
  console.log("[" + process.pid + "]: " + msg);
}