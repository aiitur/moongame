import React, {Component} from 'react';

// This one is needed to have the inline GraphQL queries with gql`` syntax
import gql from "graphql-tag";
import {Query} from "react-apollo";

const MSG_QUERY = gql`{ messages }`;

// Get Backend URL from .env file
const BACKEND_URL = process.env.REACT_APP_GRAPHQL_BACKEND_URL;

class App extends Component {
  render() {
    return (
      <Query query={MSG_QUERY}>
        {
          function({loading, error, data}) {
            if (loading) return "Loading...";
            if (error) return `Error while loading data`;
            if (!data.messages) {
              return "Invalid response structure";
            }

            return (
              <div>
                <h1>Messages stored on the server:</h1>
                <ul>
                  {data.messages.map(m => <li>{m}</li>)}
                </ul>
                <p>To play with the GrapgQL queries, open <a href={BACKEND_URL}>{BACKEND_URL}</a> in the browser.</p>
              </div>
            );
          }
        }
      </Query>
    )
      ;
  }
}

export default App;
