import React from 'react';
import ReactDOM from 'react-dom';
import App from "./components/App";

// These are necessary to create Apollo GraphQL client with HTTP transport and in-memory caching
import {createHttpLink} from 'apollo-link-http'
import {InMemoryCache} from 'apollo-cache-inmemory'
import {ApolloClient} from 'apollo-client'

// This is required to have higher-order React component that injects GraphQL data into React components
import {ApolloProvider} from 'react-apollo'

// Get Backend URL from .env file
const BACKEND_URL = process.env.REACT_APP_GRAPHQL_BACKEND_URL;

console.log(BACKEND_URL);

const httpLink = createHttpLink({
  uri: BACKEND_URL
});

const client = new ApolloClient({
  link: httpLink,
  cache: new InMemoryCache()
});


ReactDOM.render(
  <ApolloProvider client={client}>
    <App/>
  </ApolloProvider>,
  document.getElementById('root')
);
