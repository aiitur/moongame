# Different JS styles
The purpose of this project is to show different ways to define 'classes' and objects in Javascript.

# Getting started
This is a console app, that can be run with Node.js. Run the following in the terminal:
`node index.js`
