class Car {
    constructor(maker) {
        this.maker = maker;
    }

    print() {
        console.log("A car made by " + this.maker);
    }
}

module.exports = Car;