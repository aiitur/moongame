// This file shows how we can define a function and then use it as a constructor to create multiple objects

function PersonConstructor(name, age) {
    // Use this keyword to assign values to object properties. Every function, while executed, can use this to access
    // it's context
    this.name = name;
    this.age = age;

    this.print = function() {
        console.log("Person with name " + this.name + " is " + this.age + " years old");
    }
}

// We need this to tell that we export only one thing from this module: the class
module.exports = PersonConstructor;

// Alternatively, if we would like to export several things:
// module.exports = { firstExportedFuncName: firstFunction, secondFuncName: secondFunction, .... }