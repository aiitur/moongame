let o1 = require("./object-literal").object;
let PersonClass = require("./function-constructor");
let Car = require("./car-class");

console.log("Welcome to object style demo");


// Option 1) Use object literal directly
o1.fun1();

// Option 2) Use an object literal as prototype for a new object
let o2= Object.create(o1);
o2.property1 = "AnotherValue";
o2.fun1();

// Option 3) Use a constructor function to create objects
const person1 = new PersonClass("Žeņa", 13);
const person2 = new PersonClass("John Doe", 22);
person1.print();
person2.print();

// Option 4) Use ES6 class syntax
const car1 = new Car("Nissan");
const car2 = new Car("Tesla");
car1.print();
car2.print();
