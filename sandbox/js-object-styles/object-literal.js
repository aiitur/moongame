// In this file we show how to create objects using Object literals
// We don't crate a "class" here. We create a singleton object, in one copy.
// One can use this object directly, or create a new object and use this one as a prototype for the new one.
// To create anew object, use Object.create() method, see docs here:
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/create

let literal_object = {
    property1: "Value1",
    property2: 13.5,
    fun1: function() {
        console.log("function fun1() called. this has property1=" + this.property1 + ", property2=" + this.property2);
    }
};

// export default literal_object;
module.exports = {
    object: literal_object
};