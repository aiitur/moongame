# Liquibase example

Example SQL versioning with [Liquibase](http://www.liquibase.org/).

## Adding SQL changes to the log

Whenever a DB change must be made in a new commit, the developer should do the following.
### First change in this DB version
If this is the first change within this version, create a new changeset file in 
the `database` directory, name it `db-<version>.yml`.

Then extend the `db-changelogs.yml` file - include the newly created `db-<version>.yml` file there
by adding a line:
  ```
  - include:
      file: database/db-<version>.yml
  ```  

### For every change
Add your SQL changes in the `db-<version>.yml` file:
```
  - changeSet:
      id: 201
      tagDatabase:
        tag: v0.2.1
      author: kolja
      changes:
        sql: ALTER TABLE t1 ADD COLUMN name VARCHAR(32) NULL, ADD COLUMN title VARCHAR(100) NULL;
      rollback:
        - sql:
            sql: ALTER TABLE t1 DROP COLUMN name, DROP COLUMN title;
```
 
Comments:

* `id` should be a unique over all the change sets, all versions. 
Suggested ID numbering: 100 * versionNumber + autoincrement
* `tag`: unique tag for this change. Suggested naming: `release-version` + . + increment. 
For example, first DB change for version `v0.2` should be `v0.2.1`, second one: `v0.2.2`, etc.  
* author: name/nick of the person who made this change
* `sql`: raw SQL
* `rollback sql`: SQL that must be executed to roll back this change. MUST be present for each change, 
otherwise rollback won't work at all. For example, if one wants to roll back from version v3.0.5 to v2.5.1, 
and rollback is missing for v2.8.2, rollback will fail.  

 
## Running SQL changes
To update the DB to the latest state (according to this commit), run:
 `db-sync.php update`

## Rolling back to an older version
If you simply want to roll back the DB to an older version while keeping GIT at the same 
commit, execute command:
 `db-sync.php rollback <tag>`, where `tag` is the tag for specific DB change set that you want 
to revert to.

If you want to check out an older version of the system from the GIT and revert the DB to
the state as it was in that commit. There are two options.

### Option A: Clean restart
If it is ok to drop the current database and restore it to the necessary state from scratch, simply
DROP all the tables in the DB and run `db-sync.php update`.

### Option B: Revert from current state
If you want to go back from the current DB to the later version, several steps must be done:
1. Read the GIT diff for the desired commit, without checkout. Find out the latest 
DB tag in that commit.
2. Run `db-sync.php rollback <tag>` to revert to that DB version. 
3. Check out the necessary GIT commit to update the source code. 

