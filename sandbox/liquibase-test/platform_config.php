<?php

define("DEBUG", false); // Set this to true to enable some more debug output in dev environment
define("DB_HOST", "localhost");
define("DB_NAME", "lbtest");
define("DB_USER", "lbtest");
define("DB_PWD", "lbtest");
define("DB_PORT", "3306");
