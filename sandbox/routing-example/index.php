<?php

// AltoRouter test
$start_time = microtime(true);

require 'vendor/autoload.php';

class UserController
{
    public function listAction()
    {
        echo "Here are all the users...<br/>";
    }

    public function doAction($params)
    {
        $id = $params["id"];
        $action = $params["action"];
        echo "Try to $action user with ID $id <br />";
    }

    public function show($params)
    {
        $id = $params["id"];
        echo "Showing user with ID $id <br />";
    }
}

$router = new AltoRouter();

try {
    $router->setBasePath('/routing-example');
    $router->map('GET', '/', function () {
        echo "Welcome home, Mr Mandela!";
    }, 'home');
    $router->map('GET', '/users/', array('c' => 'UserController', 'a' => 'listAction'), "list_users");
    $router->map('GET', '/users/[i:id]', 'UserController#show', 'show_user');
    $router->map('POST', '/users/[i:id]/[delete|update:action]', 'UserController#doAction', 'users_do');

// match current request
    $match = $router->match();

    $home = $router->generate('home');
    $show_user = $router->generate('show_user', array('id' => 5));
    $list_users = $router->generate('list_users');
    $user_update = $router->generate('users_do', array('id' => 10, 'action' => 'update'));
    $user_delete = $router->generate('users_do', array('id' => 12, 'action' => 'delete'));

} catch (Exception $e) {
    echo "Exception: " . $e->getMessage() . "<br />";
}

processRoute($match);


// AltoRouter does not process the route directly, we have to handle it
function processRoute($match)
{
    $processed = false;
    $class_name = null;
    $method_name = null;
    $t = $match["target"];
    $p = $match["params"];

    if (is_callable($t)) {
        // Simple function - call it directly
        call_user_func_array($t, $p);
        $processed = true;
    } else if (is_array($t) && isset($t['c']) && isset($t['a'])) {
        $class_name = $t['c'];
        $method_name = $t['a'];
    } else {
        // target = class#method
        $parts = explode("#", $t);
        if (count($parts) == 2) {
            $class_name = $parts[0];
            $method_name = $parts[1];
        }
    }

    if ($class_name && $method_name) {
        if (class_exists($class_name)) {
            echo "Class $class_name exists<br/>";
            $obj = new $class_name();
            if (method_exists($obj, $method_name)) {
                echo "Method $class_name::$method_name exists<br/>";
                if (isset($match["params"]) && $match["params"]) {
                    $obj->$method_name($match["params"]);
                } else {
                    $obj->$method_name();
                }
                $processed = true;
            }
        }
    }

    if (!$processed) {
        echo "Could not call the matching function! <br/>";
    }
}

$end_time = microtime(true);
$time_elapsed = ($end_time - $start_time) * 1000000; // In milliseconds
$time_elapsed = number_format($time_elapsed, 0, ".", "");


?>
<h1>AltoRouter</h1>

<h3>Current request: </h3>
<pre>
	Target: <?php var_dump($match['target']); ?>
	Params: <?php var_dump($match['params']); ?>
	Name: 	<?php var_dump($match['name']); ?>
</pre>

<h3>Try these requests: </h3>
<p><a href="<?= $home; ?>">Home</a></p>
<p><a href="<?= $show_user; ?>">Show one user</a></p>
<p><a href="<?= $list_users; ?>">List all users</a></p>
<form action="<?= $user_update; ?>" method="post">
    <button type="submit">Update user</button>
</form>

<form action="<?= $user_delete; ?>" method="post">
    <button type="submit">Delete user</button>
</form>

Routing processed in <?= $time_elapsed; ?> microseconds