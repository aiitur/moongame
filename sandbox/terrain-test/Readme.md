## Simple terrain example using three.js

Simply open the `terrain-test.html` file.
Uses an older version of three.js. Adapted from example at 

http://thematicmapping.org/playground/webgl/terrain/wireframe/jotunheimen.html

http://blog.mastermaps.com/2013/10/terrain-building-with-threejs.html 