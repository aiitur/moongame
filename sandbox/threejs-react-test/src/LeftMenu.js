import React from "react"

export default class LeftMenu extends React.Component {
    render() {
        return <div id="left-menu"><img src="/leftmenu.png" alt="Left menu"/></div>;
    }
}