import React from "react"

export default class TopMenu extends React.Component {
    render() {
        return <div id="top-menu"><img src="/topmenu.png" alt="Top menu"/></div>;
    }
}