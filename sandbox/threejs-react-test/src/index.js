import React from 'react';
import ThreeEntryPoint from './threejs/ThreeEntryPoint';
import * as ReactDOM from "react-dom";
import TopMenu from "./TopMenu";
import LeftMenu from "./LeftMenu";

// The main React wrapper-component for the Three-js world
export default class ThreeContainer extends React.Component {
    componentDidMount() {
        ThreeEntryPoint(this.threeRootElement);
    }

    render() {
        return (
            <div>
                <TopMenu/>
                <LeftMenu/>
                <div ref={element => this.threeRootElement = element} id="threejs-container"/>
            </div>
        );
    }
}

ReactDOM.render(
    <ThreeContainer/>,
    document.getElementById("root")
);