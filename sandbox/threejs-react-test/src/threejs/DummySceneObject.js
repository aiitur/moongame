//import * as THREE from 'three';
import OBJLoader from 'three-react-obj-loader';
import Man from "../assets/models/Man.obj"


// Create a Dummy ThreeJS object that will be rendered in the scene
export default function DummySceneObject(scene) {
    /////////////////////////////////////////////
    // Constructor
    /////////////////////////////////////////////
    var dummyObj = null;
    var loader = new OBJLoader(); // TODO: This is ES6 syntax. Won't work on older browsers!
    // load a resource
    loader.load(
        // resource URL
        Man,
        // called when resource is loaded
        function(object) {
            dummyObj = object;
            scene.add(dummyObj);
        },
        // called when loading is in progresses
        function(xhr) {
            console.log(Math.round(xhr.loaded / xhr.total * 100) + '% loaded');
        },
        // called when loading has errors
        function(error) {
            console.log('An error happened');
        }
    );
    /////////////////////////////////////////////
    // EOF constructor
    /////////////////////////////////////////////

    /**
     * Render the next frame of the object.
     * @param time Time delta since the last rendered frame
     */
    function update(time) {
        if (dummyObj) {
            // dummyObj.rotation.x += 0.01;
            dummyObj.rotation.y += 0.02;
        }
    }

    return {
        update
    };
}
