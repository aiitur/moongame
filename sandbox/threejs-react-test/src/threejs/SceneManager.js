import * as THREE from 'three';
import DummySceneObject from "./DummySceneObject";

//-------------------------------------
// Static functions for internal use
//-------------------------------------
function buildCamera() {
    const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
    camera.position.z = 30;
    return camera;
}

function buildRender(canvas) {
    const renderer = new THREE.WebGLRenderer({canvas: canvas, antialias: true, alpha: true});
    renderer.setSize(canvas.width, canvas.height);
    return renderer;
}

//-------------------------------------


// Manager for ThreeJS scene(s). Currently only one scene supported
export default function SceneManager(canvas) {
    const clock = new THREE.Clock();
    const scene = new THREE.Scene();
    const renderer = buildRender(canvas);
    const camera = buildCamera(canvas);
    const sceneSubjects = [];
    // Create all the scene objects. This should be moved to Scene class once we have a separate class/object for each scene
    // The scene itself should be responsible for creating all objects in the scene
    addSceneSubject(DummySceneObject(scene));

    // Register new subject in the scene
    function addSceneSubject(subject) {
        if (!(subject in sceneSubjects)) {
            sceneSubjects.push(subject);
        }
    }

    // Render the next frame. Consider how much time spent since last update
    function update() {
        const elapsedTime = clock.getElapsedTime();
        for (let i = 0; i < sceneSubjects.length; i++) {
            sceneSubjects[i].update(elapsedTime);
        }
        renderer.render(scene, camera);
    }

    // Update the scene when windows resized
    function onWindowResize() {
    }

    // Return publicly accessible methods
    return {
        update, onWindowResize
    }
}