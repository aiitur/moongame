import SceneManager from "./SceneManager.js"

// The main context for all three.js stuff
// ThreeContainer.threeRootElement will be passed as parameter to this context, therefore

// Create a canvas element and append it as a child to the containerElement
function createCanvas(containerElement) {
    const canvas = document.createElement("canvas");
    containerElement.appendChild(canvas);
    return canvas;
}

var sceneManager = null;
var canvas = null;

// Render the three.js world
function render() {
    requestAnimationFrame(render);
    sceneManager.update();
}


// containerElement will actually be ThreeContainer.threeRootElement
export default function ThreeEntryPoint(containerElement) {
    // This code will be run once someone calls threeEntryPoint(containerElement)
    canvas = createCanvas(containerElement);
    sceneManager = SceneManager(canvas);
    bindEventListeners();
    render();
}

// Update the properties to match actual size of canvas
function onCanvasResized() {
    canvas.style.width = "100%";
    canvas.style.height = "100%";
    canvas.width = canvas.offsetWidth;
    canvas.height = canvas.offsetHeight;
    sceneManager.onWindowResize();
}

// Register handlers for window resizing, clicks etc
function bindEventListeners() {
    window.onresize = onCanvasResized;
    onCanvasResized();
}
